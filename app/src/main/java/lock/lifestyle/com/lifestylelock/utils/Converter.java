package lock.lifestyle.com.lifestylelock.utils;

import android.util.Log;


/**
 * Created by QSD on 10/12/2016.
 */

public class Converter {

    private static final String TAG = Converter.class.getSimpleName();
    private final static String HEX = "0123456789ABCDEF";
    private final static char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
            'E', 'F'};

    /**
     * Print in Log.e hex value
     *
     * @param data byte[]
     */
    public static void printHex(byte[] data, String tag) {
        if (data != null && data.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(data.length);
            for (byte byteChar : data) {
                stringBuilder.append(String.format("%02X ", byteChar));
            }
            Log.e(tag, stringBuilder.toString());
        }
    }

    /**
     * Convert Hex to byte array
     *
     * @param hexString String
     * @return byte[]
     */
    public static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
                    16).byteValue();
        return result;
    }

    /**
     * Convert byte[] to hex String
     *
     * @param buf byte[]
     * @return Hex String
     */
    public static String toHex(byte[] buf) {
        if (buf == null)
            return "";
        StringBuffer result = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++) {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }

    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public static String byteArrayToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;

        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }


    public static String bytesToDecimal(byte[] bytes) {
        char[] decimalArray = "0123456789".toCharArray();
        char[] decimalChars = new char[bytes.length * 4];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            decimalChars[j * 4 + 1] = decimalArray[(v / 10) % 10];
            decimalChars[j * 4 + 2] = decimalArray[v % 10];
        }
        return new String(decimalChars);
    }

    public static byte[] intTo32ByteArray(int number) {

        byte[] byteArray = new byte[]{
                (byte) (number >> 24),
                (byte) (number >> 16),
                (byte) (number >> 8),
                (byte) number

        };
        return byteArray;
    }

    public static int batteryLevelConvertToInt(byte[] b) {
        int value = 0;
        value = (value << 8) + (b[0] & 0xFF);
        return value;
    }


}
