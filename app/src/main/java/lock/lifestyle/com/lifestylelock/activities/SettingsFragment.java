package lock.lifestyle.com.lifestylelock.activities;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import lock.lifestyle.com.lifestylelock.R;

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        setBehavior();

        setInitialRapidAccessValue();
    }

    /**
     * Sets custom click listeners for more custom preferences (Check updates and Rapid access).
     */
    private void setBehavior() {

        findPreference(getString(R.string.key_check_updates)).setOnPreferenceClickListener(new Preference
                .OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Toast.makeText(getActivity(), "Checking for updates...", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        findPreference(getString(R.string.key_rapid_access)).setOnPreferenceChangeListener(new Preference
                .OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                updateRapidAccessPreference(getResources().getStringArray(R.array.rapid_access_values)[Integer
                        .parseInt(o.toString())]);
                return true;
            }
        });
    }

    private void updateRapidAccessPreference(String s) {
        findPreference(getString(R.string.key_rapid_access)).setSummary(s);
    }

    private void setInitialRapidAccessValue() {
        int index = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString
                (getString(R.string.key_rapid_access), "-1"));

        String summaryValue;

        if (index != -1) {
            summaryValue = getResources().getStringArray(R.array.rapid_access_values)[index];
        } else {
            summaryValue = "Unknown";
        }

        findPreference(getString(R.string.key_rapid_access)).setSummary(summaryValue);
    }
}
