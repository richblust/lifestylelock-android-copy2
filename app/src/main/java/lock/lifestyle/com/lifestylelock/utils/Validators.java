package lock.lifestyle.com.lifestylelock.utils;

import android.content.Context;
import android.text.Editable;
import android.widget.ImageView;
import android.widget.Toast;

import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;
import java.util.List;

import lock.lifestyle.com.lifestylelock.BuildConfig;
import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.model.DeviceStorage;
import lock.lifestyle.com.lifestylelock.model.Global;

/**
 * Created by QSD on 19/Oct/2016.
 * Class is used for
 */

public class Validators {


    public static boolean validateDeviceName(String deviceName, List<DeviceStorage> deviceList) {

        boolean isNameMatch = false;

        if (deviceName.length() >= 4) {
            String scannedDeviceId = deviceName.substring(deviceName.length() - 4, deviceName.length());
            for (int i = 0; i < deviceList.size(); i++) {
                if (scannedDeviceId.toUpperCase().equals(deviceList.get(i).getSerialPart().toUpperCase())) {
                    isNameMatch = true;
                } else {
                    isNameMatch = false;
                }
            }
        } else {
            isNameMatch = false;
        }

        return isNameMatch;
    }

    public static boolean validateNewAddedDevice(String deviceName, String serialPart) {
        deviceName = deviceName.substring(deviceName.length() - 4, deviceName.length());
        if (deviceName.equals(serialPart)) {
            return true;
        } else {
            return false;
        }

    }


    // Validate device serial number and device name
    public static boolean validateDeviceData(String serial, String deviceName, Context context) {

        if (serial.contains("-")) {
            serial = serial.replace("-", "");
        }

        if (deviceName.isEmpty() && serial.isEmpty()) {
            Boast.makeText(context, "All fields are empty, please fill data!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (deviceName.isEmpty()) {
            Boast.makeText(context, "Device name is empty, please insert device name!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (serial.isEmpty()) {
            Boast.makeText(context, "Serial is empty, please insert device serial!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (serial.length() < 12 || serial.length() > 12) {
            Boast.makeText(context, "Serial must have 12 characters!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (serial.length() == 12 && !deviceName.isEmpty()) {
            return true;

        } else {
            if (!deviceName.isEmpty()) {
                Boast.makeText(context, "Serial must have 12 characters!", Toast.LENGTH_SHORT).show();
            } else {
                Boast.makeText(context, "Please insert device name!", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    }


    // Used to validate serial number
    public static Editable formatSerialInput(Editable s, int length_before) {
        if (length_before < s.length()) {
            if (s.length() == 4 || s.length() == 9)
                s.append("-");
            if (s.length() > 4) {
                if (Character.isDigit(s.charAt(4)))
                    s.insert(4, "-");
            }
            if (s.length() > 8) {
                if (Character.isDigit(s.charAt(9)))
                    s.insert(9, "-");
            }
        }

        if (length_before > s.length()) {
            if (s.length() == 4) {
                s.delete(3, 4);
            }
            if (s.length() == 9) {
                s.delete(8, 9);
            }
        }

        return s;
    }


    public static boolean containSerial(List<DeviceStorage> deviceList, String serial) {

        for (int i = 0; i < deviceList.size(); i++) {
            if (deviceList.get(i).getSerial().equals(serial)) {
                return true;
            }
            return false;
        }
        return false;
    }


    public static boolean containAddress(List<DeviceStorage> deviceList, String address) {

        boolean isContain = false;

        for (int i = 0; i < deviceList.size(); i++) {
            if (deviceList.get(i).getMacAddress().equals(address)) {
                isContain = true;
            }
        }
        return isContain;
    }

    public static DeviceStorage getDeviceByAddress(List<DeviceStorage> deviceList, String macAddress) {
        for (int i = 0; i < deviceList.size(); i++) {
            if (deviceList.get(i).getMacAddress().equals(macAddress)) {
                return deviceList.get(i);
            }

        }
        return null;
    }


    public static void updateDeviceStatus(DeviceStorage device) {
        if (!Global.savedDevices.isEmpty()) {
            for (int i = 0; i < Global.savedDevices.size(); i++) {
                if (Global.savedDevices.get(i).getMacAddress().equals(device.getMacAddress())) {
                    Global.savedDevices.get(i).setDeviceLockState(device.getDeviceLockState());
                }
            }
        }
    }


    public static void updateDeviceBatteryStatus(DeviceStorage device, byte[] batteryData) {
        if (!Global.savedDevices.isEmpty()) {
            for (int i = 0; i < Global.savedDevices.size(); i++) {
                if (Global.savedDevices.get(i).getMacAddress().equals(device.getMacAddress())) {
                    Global.savedDevices.get(i).setBatteryLevel(batteryData);
                }
            }
        }
    }


    public static String encryptPin(String pin) {
        String messageBeforeDecrypt = null;
        try {
            messageBeforeDecrypt = AESCrypt.encrypt(BuildConfig.LOCK_KEY, pin);
        } catch (GeneralSecurityException e) {
            //handle error - could be due to incorrect password or tampered encryptedMsg
        }

        return messageBeforeDecrypt;
    }


    public static String decryptPin(String encryptedHash) {
        String messageAfterDecrypt = null;
        try {
            messageAfterDecrypt = AESCrypt.decrypt(BuildConfig.LOCK_KEY, encryptedHash);
        } catch (GeneralSecurityException e) {
            //handle error - could be due to incorrect password or tampered encryptedMsg
        }

        return messageAfterDecrypt;
    }


    public static void setBatteryImage(ImageView imageView, int batteryLevel) {
        if (batteryLevel == 100) {
            imageView.setImageResource(R.drawable.ic_battery_full);
        } else if (batteryLevel < 100 && batteryLevel >= 90) {
            imageView.setImageResource(R.drawable.ic_battery_90);
        } else if (batteryLevel < 90 && batteryLevel >= 80) {
            imageView.setImageResource(R.drawable.ic_battery_80);
        } else if (batteryLevel < 80 && batteryLevel >= 70) {
            imageView.setImageResource(R.drawable.ic_battery_70);
        } else if (batteryLevel < 70 && batteryLevel >= 60) {
            imageView.setImageResource(R.drawable.ic_battery_60);
        } else if (batteryLevel < 60 && batteryLevel >= 50) {
            imageView.setImageResource(R.drawable.ic_battery_half);
        } else if (batteryLevel == 50) {
            imageView.setImageResource(R.drawable.ic_battery_half);
        } else if (batteryLevel < 50 && batteryLevel >= 40) {
            imageView.setImageResource(R.drawable.ic_battery_40);
        } else if (batteryLevel < 40 && batteryLevel >= 30) {
            imageView.setImageResource(R.drawable.ic_battery_30);
        } else if (batteryLevel < 30 && batteryLevel >= 20) {
            imageView.setImageResource(R.drawable.ic_battery_20);
        } else if (batteryLevel < 20 && batteryLevel >= 10) {
            imageView.setImageResource(R.drawable.ic_battery_10);
        } else if (batteryLevel < 10) {
            imageView.setImageResource(R.drawable.ic_battery_10);
        }
    }


}
