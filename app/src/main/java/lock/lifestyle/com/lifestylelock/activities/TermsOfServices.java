package lock.lifestyle.com.lifestylelock.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.storage.SpHelper;

public class TermsOfServices extends AppCompatActivity implements View.OnClickListener {

    Button btnAgree, btnDisagree;
    Context mContext;
    Integer counter = 0;
    WebView webTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_services);
        mContext = this;

        btnAgree = (Button) findViewById(R.id.first_start_agree);
        btnDisagree = (Button) findViewById(R.id.first_start_disagree);
        btnAgree.setOnClickListener(this);
        btnDisagree.setOnClickListener(this);

        webTerms = (WebView) findViewById(R.id.first_start_webview);
        webTerms.loadUrl("file:///android_asset/TermsOfService.html");
        //same as background color space_color
        webTerms.setBackgroundColor(Color.parseColor("#211f20"));
        counter++;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.first_start_agree:

                switch (counter) {
                    case 1:
                        webTerms.loadUrl("file:///android_asset/TermsOfUse.html");
                        webTerms.setBackgroundColor(Color.parseColor("#211f20"));
                        counter++;
                        break;
                    case 2:
                        SpHelper.setSharedPreferenceBoolean(mContext, SpHelper.FIRST_TIME, false);
                        Intent intent = new Intent(TermsOfServices.this, AuthActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                }
                break;

            case R.id.first_start_disagree:

                new AlertDialog.Builder(mContext)
                        .setMessage("Installation and use of this application requires acceptance of Terms of Use.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();

                break;

        }
    }
}
