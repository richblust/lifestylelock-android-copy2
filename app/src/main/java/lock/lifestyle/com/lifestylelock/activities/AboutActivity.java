package lock.lifestyle.com.lifestylelock.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;

import lock.lifestyle.com.lifestylelock.BuildConfig;
import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.adapters.AboutListAdapter;
import lock.lifestyle.com.lifestylelock.model.Authentication;
import lock.lifestyle.com.lifestylelock.storage.SpHelper;
import lock.lifestyle.com.lifestylelock.utils.Constants;
import lock.lifestyle.com.lifestylelock.utils.WebHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class AboutActivity extends AppCompatActivity {

    WebHelper webSite;
    private ListView aboutList;
    private ArrayList<String> aboutDataList = new ArrayList<>();
    boolean isUserAuth;
    TextView versionNumber;
    Context mContext;
    AboutListAdapter adapter;
    Authentication auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFont();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        mContext = this;


        initUI();
    }

    private void initUI() {

        auth = new Authentication(this);
        isUserAuth = SpHelper.getSharedPreferenceBoolean(this, SpHelper.USER_AUTH, false);

        populateList();

        versionNumber = (TextView) findViewById(R.id.version_number);
        versionNumber.setText(BuildConfig.VERSION_NAME.toString());
        webSite = new WebHelper();

        adapter = new AboutListAdapter(this, aboutDataList);
        aboutList = (ListView) findViewById(R.id.aboutListView);
        aboutList.setAdapter(adapter);

        aboutList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        termsOfUse();
                        break;
                    case 1:
                        privacyPolicy();
                        break;
                    case 2:
                        caPrivacyPolicy();
                        break;
                    case 3:
                        changePassword();
                        break;
                    case 4:
                        removePinDialog();
                        break;
                }
            }
        });

    }

    private void populateList() {
        aboutDataList.add("Terms of Use");
        aboutDataList.add("Privacy Policy");
        aboutDataList.add("California Privacy Policy");


        if (!isUserAuth) {
            aboutDataList.add("Add PIN");
        } else {
            aboutDataList.add("Change PIN");
            aboutDataList.add("Remove PIN");
        }
    }

    private void termsOfUse() {
        Intent i = new Intent(this, TermsOfUseActivity.class);
        startActivity(i);
    }

    private void privacyPolicy() {
        webSite.goToBuyStore(this, Uri.parse(getString(R.string.privacy_policy_url)));
    }

    private void caPrivacyPolicy() {
        webSite.goToBuyStore(this, Uri.parse(getString(R.string.ca_privacy_policy_url)));
    }

    private void changePassword() {
        Intent intent = new Intent(AboutActivity.this, AuthActivity.class);
        intent.putExtra("is_pa" +
                "ssword_update", true);
        startActivity(intent);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath(Constants.getFontName())
                .setFontAttrId(R.attr.fontPath).build());
    }

    private void refreshList() {
        isUserAuth = SpHelper.getSharedPreferenceBoolean(mContext, SpHelper.USER_AUTH, false);
        aboutDataList.clear();
        populateList();
        adapter = new AboutListAdapter(mContext, aboutDataList);
        adapter.notifyDataSetChanged();
        aboutList.setAdapter(adapter);
    }

    private void removePinDialog() {
        new AlertDialog.Builder(mContext)
                .setTitle("Pin delete")
                .setMessage("Do you want to delete pin")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        auth.removePin();
                        refreshList();
                    }
                })
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();


    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
