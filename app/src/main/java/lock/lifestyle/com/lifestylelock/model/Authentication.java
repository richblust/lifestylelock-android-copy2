package lock.lifestyle.com.lifestylelock.model;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import lock.lifestyle.com.lifestylelock.storage.SpHelper;
import lock.lifestyle.com.lifestylelock.utils.Boast;
import lock.lifestyle.com.lifestylelock.utils.Validators;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by QSD on 15/Nov/2016.
 * Class is used for
 */

public class Authentication {

    Context mContext;

    public Authentication(Context context) {
        mContext = context;
    }


    public boolean addNewPin(String pin, String pinConfirm) {
        boolean isMatch;


        if (pin.isEmpty() || pinConfirm.isEmpty()) {
            Boast.makeText(mContext, "Please insert pin data!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (pin.length() < 4) {
            Boast.makeText(mContext, "Pin value have minimum 4 digits!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (pinConfirm.length() < 4) {
            Boast.makeText(mContext, "Pin confirm value have minimum 4 digits!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!pin.equals(pinConfirm)) {
            Boast.makeText(mContext, "Pin confirm value do not match!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (pin.equals(pinConfirm)) {
            //layout.setVisibility(View.GONE);

            isMatch = true;
            SpHelper.setSharedPreferenceString(mContext, SpHelper.USER_AUTH_KEY, Validators.encryptPin(pin));
            SpHelper.setSharedPreferenceBoolean(mContext, SpHelper.USER_AUTH, true);


        } else {
            isMatch = false;
        }

        return isMatch;
    }


    public boolean isPinValid(String pin) {


        if (pin.isEmpty()) {
            Boast.makeText(mContext, "Please insert pin data!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (pin.length() < 4) {
            Boast.makeText(mContext, "Pin value have minimum 4 digits!", Toast.LENGTH_SHORT).show();
            return false;
        }

        //String pinSp = SpHelper.getSharedPreferenceString(mContext,SpHelper.USER_AUTH_KEY,null);
        String pinSp = Validators.decryptPin(Validators.encryptPin(pin));
        if (pinSp.equals(pin)) {
            return true;
        } else {
            Boast.makeText(mContext, "Pin value is not correct!", Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    public void onSkipPress() {
        SpHelper.setSharedPreferenceBoolean(mContext, SpHelper.USER_AUTH, false);
    }

    public void hideOtherLayouts(Toolbar toolbar, RelativeLayout deviceContainer) {
        toolbar.setVisibility(GONE);
        deviceContainer.setVisibility(GONE);

    }

    public void showOtherLayouts(Toolbar toolbar, RelativeLayout deviceContainer) {
        toolbar.setVisibility(VISIBLE);
        deviceContainer.setVisibility(VISIBLE);
    }

    public void hideAuthLayout(LinearLayout layout) {
        layout.setVisibility(GONE);
    }


    public void showAuthLayout(LinearLayout layout) {
        layout.setVisibility(View.VISIBLE);
    }


    public void removePin() {
        SpHelper.removeKey(mContext, SpHelper.USER_AUTH_KEY);
        SpHelper.setSharedPreferenceBoolean(mContext, SpHelper.USER_AUTH, false);
    }


    public void changePin(String pin) {
        if (pin.length() == 4) {
            SpHelper.setSharedPreferenceString(mContext, SpHelper.USER_AUTH_KEY, pin);
        } else {
            Toast.makeText(mContext, "Pin must have 4 digit", Toast.LENGTH_SHORT).show();
        }
    }


}
