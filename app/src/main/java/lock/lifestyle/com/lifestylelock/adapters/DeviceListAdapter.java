package lock.lifestyle.com.lifestylelock.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import java.util.List;

import lock.lifestyle.com.lifestylelock.BuildConfig;
import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.activities.MainActivity;
import lock.lifestyle.com.lifestylelock.interfaces.ImageClicker;
import lock.lifestyle.com.lifestylelock.interfaces.SwipeHandler;
import lock.lifestyle.com.lifestylelock.model.DeviceStorage;
import lock.lifestyle.com.lifestylelock.utils.Boast;
import lock.lifestyle.com.lifestylelock.utils.Constants;
import lock.lifestyle.com.lifestylelock.utils.Converter;
import lock.lifestyle.com.lifestylelock.utils.Validators;

/**
 * Created by QSD on 9/20/2016.
 */

public class DeviceListAdapter extends BaseSwipeAdapter {
    ImageClicker mImageClicker;
    SwipeHandler mSwipeHandler;
    Context mContext;
    private List<DeviceStorage> mLeDevices;

    public DeviceListAdapter(Context context, List<DeviceStorage> devices) {

        mContext = context;
        mImageClicker = (MainActivity) context;
        mSwipeHandler = (MainActivity) context;
        mLeDevices = devices;
    }

    public void clearData() {
        // clear the data
        if (mLeDevices != null && !mLeDevices.isEmpty()) {
            mLeDevices.clear();
        }
    }

    public void clear() {
        mLeDevices.clear();
    }

    @Override
    public int getCount() {
        return mLeDevices.size();
    }

    @Override
    public Object getItem(int position) {
        return mLeDevices.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.device_list_item_swipelayout;
    }

    @Override
    public View generateView(int i, ViewGroup convertView) {
        return LayoutInflater.from(mContext).inflate(R.layout.device_list_item, null);

    }

    @Override
    public void fillValues(int i, final View convertView) {

        TextView txtDeviceName = (TextView) convertView.findViewById(R.id.device_name);
        TextView txtDeviceStatus = (TextView) convertView.findViewById(R.id.list_item_device_state_text);
        TextView txtBatteryLevel = (TextView) convertView.findViewById(R.id.list_item_battery_level_text);
        ImageView imgDeviceState = (ImageView) convertView.findViewById(R.id.list_item_device_state);
        ImageView imgSettings = (ImageView) convertView.findViewById(R.id.list_item_settings);
        ImageView imgBattery = (ImageView) convertView.findViewById(R.id.list_item_battery_level);
        ImageView imgHistoryLog = (ImageView) convertView.findViewById(R.id.list_item_history);
        ProgressBar pgbProgress = (ProgressBar) convertView.findViewById(R.id.list_item_status_progress);
        final SwipeLayout swipeLayout = (SwipeLayout) convertView.findViewById(R.id.device_list_item_swipelayout);
        LinearLayout layoutRename = (LinearLayout) convertView.findViewById(R.id.bottom_l_wrapper);
        LinearLayout layoutRemove = (LinearLayout) convertView.findViewById(R.id.bottom_r_wrapper);
        RelativeLayout layoutLockContainer = (RelativeLayout) convertView.findViewById(R.id.device_list_lock_container);

        final DeviceStorage device = mLeDevices.get(i);
        final String deviceNameVal = device.getName();
        if (txtDeviceName != null && txtDeviceName.length() > 0) {
            txtDeviceName.setText(deviceNameVal);
        } else {
            txtDeviceName.setText(R.string.unknown_device);
        }

        if (device.getBatteryLevel() != null) {

            Validators.setBatteryImage(imgBattery,Converter.batteryLevelConvertToInt(device.getBatteryLevel()));
            imgBattery.setVisibility(View.VISIBLE);
            txtBatteryLevel.setVisibility(View.VISIBLE);
            txtBatteryLevel.setText(Converter.bytesToDecimal(device.getBatteryLevel()) + " %");
        }

        imgSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                Intent myIntent = new Intent(mContext, SettingsActivity.class);
                //                mContext.startActivity(myIntent);
                Boast.makeText(mContext, "Settings screen is coming soon!", Toast.LENGTH_SHORT).show();
            }
        });

        imgHistoryLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Boast.makeText(mContext, "History log is coming soon!", Toast.LENGTH_SHORT).show();
            }
        });

        imgDeviceState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageClicker.onClicked(device.getMacAddress());
            }
        });

        layoutRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSwipeHandler.onRemove(device.getMacAddress());
                swipeLayout.close();
            }
        });
        layoutRename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSwipeHandler.onRename(device.getMacAddress());
                swipeLayout.close();
            }
        });

        switch (device.getDeviceStatus()) {
            case AUTH:
                txtDeviceStatus.setText("");
                txtDeviceStatus.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color.disabled_colour));
                imgDeviceState.setImageResource(R.drawable.ic_autentification);
                pgbProgress.setVisibility(View.GONE);
                layoutLockContainer.setBackgroundResource(R.drawable.auth_bg);
                imgDeviceState.setClickable(true);
                setBatteryStatus(View.GONE, imgBattery, txtBatteryLevel);
                break;
            case LOCKED:
                if (BuildConfig.FLAVOR.equals(Constants.craftsmanFlavour)) {
                    txtDeviceStatus.setText("LOCKED");
                }

                if (BuildConfig.FLAVOR.equals(Constants.lifestyleFlavour)) {
                    txtDeviceStatus.setText("Locked");
                }

                txtDeviceStatus.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color
                        .locked_text_color));
                imgDeviceState.setImageResource(R.drawable.ic_locked);
                pgbProgress.setVisibility(View.GONE);
                layoutLockContainer.setBackgroundResource(R.drawable.locked_bg);
                imgDeviceState.setClickable(true);
                if (imgBattery.getVisibility() != View.VISIBLE || txtBatteryLevel.getVisibility() != View.VISIBLE) {
                    setBatteryStatus(View.VISIBLE, imgBattery, txtBatteryLevel);
                }
                break;
            case IN_PROGRESS:
                txtDeviceStatus.setText("");
                imgDeviceState.setImageResource(android.R.color.transparent);
                pgbProgress.setVisibility(View.VISIBLE);
                imgDeviceState.setClickable(false);
                break;
            case UNLOCKED:

                if (BuildConfig.FLAVOR.equals(Constants.craftsmanFlavour)) {
                    txtDeviceStatus.setText("UNLOCKED");
                }

                if (BuildConfig.FLAVOR.equals(Constants.lifestyleFlavour)) {
                    txtDeviceStatus.setText("Unlocked");
                }

                txtDeviceStatus.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color
                        .unlocked_text_color));
                imgDeviceState.setImageResource(R.drawable.ic_unlocked);
                pgbProgress.setVisibility(View.GONE);
                layoutLockContainer.setBackgroundResource(R.drawable.unlocked_bg);
                imgDeviceState.setClickable(true);
                if (imgBattery.getVisibility() != View.VISIBLE || txtBatteryLevel.getVisibility() != View.VISIBLE) {
                    setBatteryStatus(View.VISIBLE, imgBattery, txtBatteryLevel);
                }
                break;

            case WAIT:
                imgDeviceState.setClickable(false);
                txtDeviceStatus.setText("WAIT");
                layoutLockContainer.setBackgroundResource(R.drawable.auth_bg);
                txtDeviceStatus.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color.disabled_colour));



                break;


        }


    }


    private void setBatteryStatus(int VISIBILITY, ImageView imgBattery, TextView tvBattery) {
        imgBattery.setVisibility(VISIBILITY);
        tvBattery.setVisibility(VISIBILITY);
    }


    static class ViewHolder {
        TextView deviceName;
        TextView deviceStatus;
        TextView batteryLevel;
        ImageView deviceState;
        ImageView imgBattery;
        ImageView imgSettings;
        ImageView deviceSettings;
        ImageView deviceHistory;
        ProgressBar progressBar;
        // SwipeLibrary
        SwipeLayout swipeLayout;
        RelativeLayout rename;
        RelativeLayout remove;
        LinearLayout lockContainer;
    }

}

