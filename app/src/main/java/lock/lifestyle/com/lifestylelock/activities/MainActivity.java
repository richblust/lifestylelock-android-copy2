package lock.lifestyle.com.lifestylelock.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import lock.lifestyle.com.lifestylelock.BuildConfig;
import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.adapters.DeviceListAdapter;
import lock.lifestyle.com.lifestylelock.barcode.IntentIntegrator;
import lock.lifestyle.com.lifestylelock.barcode.IntentResult;
import lock.lifestyle.com.lifestylelock.interfaces.ImageClicker;
import lock.lifestyle.com.lifestylelock.interfaces.SwipeHandler;
import lock.lifestyle.com.lifestylelock.model.Authentication;
import lock.lifestyle.com.lifestylelock.model.DeviceStorage;
import lock.lifestyle.com.lifestylelock.model.Global;
import lock.lifestyle.com.lifestylelock.services.BluetoothLeService;
import lock.lifestyle.com.lifestylelock.storage.SpHelper;
import lock.lifestyle.com.lifestylelock.utils.Boast;
import lock.lifestyle.com.lifestylelock.utils.Constants;
import lock.lifestyle.com.lifestylelock.utils.Converter;
import lock.lifestyle.com.lifestylelock.utils.CustomKeyboard;
import lock.lifestyle.com.lifestylelock.utils.GattReceiverHelper;
import lock.lifestyle.com.lifestylelock.utils.Permission;
import lock.lifestyle.com.lifestylelock.utils.Validators;
import lock.lifestyle.com.lifestylelock.utils.WebHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.view.View.GONE;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_AUTHENTICATION_CHALLENGE;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_AUTHENTICATION_HASH;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_BATTERY;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_FIRMWARE;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_HARDWARE_VERSION;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_LOCK_QUERY;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_TIME_SYNC;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_TIME_ZONE_OFFSET;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LS_BATTERY;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LS_DEVICE_INFORMATION;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LS_LOCK;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LS_TIME_SYNC;
import static lock.lifestyle.com.lifestylelock.model.Global.currentDevice;
import static lock.lifestyle.com.lifestylelock.model.Global.savedDevices;

@TargetApi (Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity implements View.OnClickListener, ImageClicker, SwipeHandler {

    public final static int REQUEST_ENABLE_BT = 1221;
    private final static String TAG = MainActivity.class.getSimpleName();
    BroadcastReceiver btReceiver;
    IntentFilter filter;
    EditText etDeviceName, etDeviceSerial, etPin;
    ImageView imgBarcodeScanner, imgOpenWebsite, imgDialogConfirm, imgOpenAbout;
    View dialogLayout;
    CustomKeyboard mCustomKeyboard;
    LinearLayout layoutAlert, layoutConnecting, layoutLogin;
    RelativeLayout layoutDevice;
    private BluetoothManager mBluetoothManager;
    private LocationManager mLocationManager;
    private BluetoothAdapter mBluetoothAdapter;
    private DeviceListAdapter mLeDeviceListAdapter;
    private BluetoothLeService mBluetoothLeService;
    private String mDeviceAddress;
    private Permission permission;
    private GattReceiverHelper gattReceiverHelper;
    private boolean mScanning;
    private Handler mHandler;
    private Button btnAddNewDevice, btnConfirmPin;
    private ListView lstDevice;
    private Context mContext;
    private Toolbar toolbar;

    // For BT LE with API >21+
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private ScanCallback mScanCallback21;
    private ScanCallback newDeviceScanCallback21;
    private BluetoothAdapter.LeScanCallback newDeviceScanCallback;
    private Authentication auth;
    private boolean isFirstAuthCreated;


    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();
            final String address = intent.getStringExtra("device_address");

            switch (action) {

                //1. When device is connected
                case Constants.ACTION_GATT_CONNECTED:
                    Log.i(TAG, "Device is connected");
                    // isConnected = true;
                    break;

                //#. When device is disconnected
                case Constants.ACTION_GATT_DISCONNECTED:
                    Log.i(TAG, "Device is disconnected");
//                    gattReceiverHelper.updateDeviceState(address,savedDevices, DeviceStorage.DeviceStatus.AUTH);

                    currentDevice.setAuthenticated(false);
                    currentDevice.setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
                    if(address!=null) {
                        for (int i = 0; i < savedDevices.size(); i++) {
                            if (savedDevices.get(i).getMacAddress().equals(address)) {
                                savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
                                savedDevices.get(i).setFirstRun(true);
                                savedDevices.get(i).setHardware(null);
                                savedDevices.get(i).setFirmware(null);
                                savedDevices.get(i).setAuthenticated(false);
                            }
                        }
                    }


                    mLeDeviceListAdapter.notifyDataSetChanged();
                    // isConnected = false;
                    break;

                //#. When data are recived
                case Constants.ACTION_DATA_AVAILABLE:
                    final byte[] data = intent.getByteArrayExtra("byteData");
                    final String actionName = intent.getStringExtra("action_name");

                    switch (actionName) {
//                        case LC_BATTERY:
//                            Validators.updateDeviceBatteryStatus(currentDevice, data);
//                            mLeDeviceListAdapter.notifyDataSetChanged();
//                            //Boast.makeText(context, "Battery notification received!", Toast.LENGTH_SHORT).show();
//
//                            break;

                        case LC_LOCK_QUERY:

                            currentDevice.setDeviceLockState(data);


                            for (int i = 0; i < savedDevices.size(); i++) {
//                                 if(!savedDevices.get(i).getMacAddress().equals(currentDevice.getMacAddress())){
                                savedDevices.get(i).setDeviceStatus(savedDevices.get(i).get_tempStatus());
                                savedDevices.get(i).set_tempStatus(null);
                              //  }
                            }

                            gattReceiverHelper.lockStateUpdater(currentDevice.getMacAddress(), Global.savedDevices);


                            mLeDeviceListAdapter.notifyDataSetChanged();
//                            Boast.makeText(context, "Lock query notification received !", Toast.LENGTH_SHORT).show();
                            break;
                    }


                    Converter.printHex(data, "Notification data: ");

                    break;

                //2. When device discover services
                case Constants.ACTION_GATT_SERVICES_DISCOVERED:
                    Log.i(TAG, "Device Services are discovered");

                    if (!currentDevice.isAuthenticated()) {
                        mBluetoothLeService.readCharacteristic(LS_LOCK, LC_AUTHENTICATION_CHALLENGE);
                    }
                    break;

                //3. Lock challenge is read, now write client challenge into lock
                case Constants.ACTION_CLIENT_CHALLENGE_WRITE:
                    mBluetoothLeService.writeCharacteristic(currentDevice.getClientChallenge(), LS_LOCK,
                            LC_AUTHENTICATION_CHALLENGE);
                    break;

                //4. Client challenge is written into lock, now authenticate with hash
                case Constants.ACTION_WRITE_ENCRYPTED_HASH:
                    mBluetoothLeService.writeCharacteristic(currentDevice.getEncryptedHash(), LS_LOCK,
                            LC_AUTHENTICATION_HASH);
                    break;


                //5. Read encrypted hash from device then compare and decrypt then auth
                case Constants.ACTION_AUTHENTICATION_MERGE:
                    mBluetoothLeService.readCharacteristic(LS_LOCK, LC_AUTHENTICATION_HASH);
                    break;


                //6. Authentication is confirmed, now add device and lets read battery level
                case Constants.ACTION_AUTHENTIFICATION_CONFIRMED:
                    Log.i(TAG, "Authentication confirmed");

                    if (Validators.getDeviceByAddress(savedDevices, address) == null) {
                        addNewDevice();
                    }

                    mBluetoothLeService.readCharacteristic(LS_BATTERY, LC_BATTERY);
                    break;

                //7. Battery level is read now reading device firmware
                case Constants.ACTION_SET_BATTERY_LEVEL:
                    mLeDeviceListAdapter.notifyDataSetChanged();
                    mBluetoothLeService.readCharacteristic(LS_DEVICE_INFORMATION, LC_FIRMWARE);
                    break;


                //8. Device firmware is read now reading hardware version
                case Constants.ACTION_READ_FIRMWARE:
                    mBluetoothLeService.readCharacteristic(LS_DEVICE_INFORMATION, LC_HARDWARE_VERSION);
                    break;


                //9. Hardware version is set. Now we writing UTC time to device
                case Constants.ACTION_UTC_TIME:
                    int unixTime = (int) (System.currentTimeMillis() / 1000);
                    byte[] currentDate = Converter.intTo32ByteArray(unixTime);

                    mBluetoothLeService.writeCharacteristic(currentDate, LS_TIME_SYNC, LC_TIME_SYNC);
                    break;


                //10. UTC time is written, now we writing UTC offset
                case Constants.ACTION_UTC_OFFSET:
                    TimeZone tz = TimeZone.getDefault();
                    Date timeNow = new Date();
                    int offsetFromUtc = tz.getOffset(timeNow.getTime()) / 1000;
                    byte[] offsetFromUtcByteArray = Converter.intTo32ByteArray(offsetFromUtc);

                    mBluetoothLeService.writeCharacteristic(offsetFromUtcByteArray, LS_TIME_SYNC, LC_TIME_ZONE_OFFSET);
                    break;


                //11. If this is first run we just read lock state in other case we unlock/lock device and continue flow
                case Constants.ACTION_LOCK_UNLOCK:
                    if (!currentDevice.isFirstRun()) {
                        gattReceiverHelper.lockOrUnlock(mBluetoothLeService);

                    } else {
                        mBluetoothLeService.readCharacteristic(LS_LOCK, LC_LOCK_QUERY);
                        currentDevice.setFirstRun(false);
                    }
                    break;


                //12. Reading lock state (Locked / Unlocked)
                case Constants.ACTION_LOCK_STATE_QUERY:
                    mBluetoothLeService.readCharacteristic(LS_LOCK, LC_LOCK_QUERY);
                    break;

                //13. Lock state is read, now update lock state
                case Constants.ACTION_LOCK_STATE_CHANGE:
                    gattReceiverHelper.lockStateUpdater(currentDevice.getMacAddress(), Global.savedDevices);
                    mLeDeviceListAdapter.notifyDataSetChanged();
                    if (layoutConnecting != null && layoutConnecting.getVisibility() == View.VISIBLE) {
                        changeConnectingView(View.GONE);
                    }

                    if (!currentDevice.isSubscribed()) {
                        mBluetoothLeService.setCharacteristicNotification(mBluetoothLeService
                                .getCharacteristicFromServices(LS_LOCK, LC_LOCK_QUERY), true);
                        Log.i(TAG, "LC_LOCK is subscribed!");
                    }
                    break;


                case Constants.ACTION_SUBSCRIBE_BATTERY:
//                    if (!currentDevice.isSubscribed()) {
//                        mBluetoothLeService.setCharacteristicNotification(mBluetoothLeService
//                                .getCharacteristicFromServices(LS_BATTERY, LC_BATTERY), true);
//                        Log.i(TAG, "LC_BATTERY is subscribed!");
//                    }


                    currentDevice.setSubscribed(true);
                    break;

            }

        }
    };

    private void changeConnectingView(int gone) {
        layoutConnecting.setVisibility(gone);
        btnAddNewDevice.setVisibility(View.VISIBLE);
        lstDevice.setVisibility(View.VISIBLE);
    }


    // Scan device service API 21+
    private ScanCallback getNewScanCallback() {
        if (mScanCallback21 == null) {
            mScanCallback21 = new ScanCallback() {

                @Override
                public void onScanResult(int callbackType, final ScanResult result) {

                    //validateDeviceOnScan(result.getDevice());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            validateDeviceOnScan(result.getDevice());
                        }
                    });

                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    for (ScanResult sr : results) {
                        Log.i("ScanResult - Results", sr.toString());
                    }
                }

                @Override
                public void onScanFailed(int errorCode) {
                    Log.e("Scan Failed", "Error Code: " + errorCode);
                }


            };
        }
        return mScanCallback21;
    }


    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    validateDeviceOnScan(device);
                }
            });

//            validateDeviceOnScan(device);
        }

    };


    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                //  isConnected = false;
                finish();
            }





//            if (!savedDevices.isEmpty()) {
//                for (int i = 0; i < savedDevices.size(); i++) {
//                    //savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
//                    //savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.IN_PROGRESS);
//                    currentDevice = savedDevices.get(i);
//                    mBluetoothLeService.connect(savedDevices.get(i).getMacAddress());
//
//                }
//            }

            Log.e(TAG, " mServiceConnection Device is connected!");
            //  isConnected = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };





    private void validateDeviceOnScan(BluetoothDevice device) {
        if (!savedDevices.isEmpty()) {
            if (device.getName() != null) {
                //Log.i(TAG, "Device name: " + device.getName());

                if (Validators.validateDeviceName(device.getName(), savedDevices)) {

                    if (!Validators.containAddress(savedDevices, device.getAddress())) {
                        savedDevices.add(currentDevice);
                        mLeDeviceListAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    private void addNewDevice() {
        Global.savedDevices.add(currentDevice);
        SpHelper.setSharedPreferenceString(mContext, SpHelper.DEVICE_KEY, new Gson().toJson(Global.savedDevices));
        //Boast.makeText(mContext, "New device is found. Connecting...", Toast.LENGTH_SHORT).show();
        mLeDeviceListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFont();
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isFirstAuthCreated = extras.getBoolean("is_first");
        }

        init();


        askForPermission();
        mContext = this;

        initBTReceiver();
        registerReceiver(btReceiver, filter);

        checkPermissions();
        initConnectionService();


    }

    private void initConnectionService() {
        if (!BluetoothLeService.isInstanceCreated()) {
            Intent gattServiceIntent = new Intent(MainActivity.this, BluetoothLeService.class);
            bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath(Constants.getFontName())
                .setFontAttrId(R.attr.fontPath).build());
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Title");
        setSupportActionBar(toolbar);
    }

    public void init() {

        initGlobalDevices();
        initConnectionService();


        auth = new Authentication(this);



        permission = new Permission(this);
        gattReceiverHelper = new GattReceiverHelper();

        mHandler = new Handler();

        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        layoutAlert = (LinearLayout) findViewById(R.id.alert_container);
        layoutDevice = (RelativeLayout) findViewById(R.id.device_container);
        layoutConnecting = (LinearLayout) findViewById(R.id.device_connecting_alert);
        layoutLogin = (LinearLayout) findViewById(R.id.login_main);
        etPin = (EditText) findViewById(R.id.et_new_pin);
        etPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }else {
                    showKeyboard(etPin);
                }


            }
        });

        setKeyboardDelay(etPin);

        initToolBar();


        boolean isUserAuth = SpHelper.getSharedPreferenceBoolean(this, SpHelper.USER_AUTH, false);
        if (!isUserAuth || isFirstAuthCreated) {
            layoutLogin.setVisibility(GONE);
            auth.showOtherLayouts(toolbar,layoutDevice);
        }


        if (Build.VERSION.SDK_INT >= 21) {
            getNewScanCallback();
        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        if (mBluetoothManager != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<ScanFilter>();
            }
            scanLeDevice(true);
        }


        imgOpenWebsite = (ImageView) findViewById(R.id.buy_lock_button);
        imgOpenWebsite.setOnClickListener(this);

        btnAddNewDevice = (Button) findViewById(R.id.add_new_device);
        btnConfirmPin = (Button) findViewById(R.id.button_confirm_new_pin);
        btnAddNewDevice.setOnClickListener(this);
        btnConfirmPin.setOnClickListener(this);
        if (BuildConfig.DEBUG) {
            btnConfirmPin.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    etPin.setText("1234");
                    return false;
                }
            });
        }

        imgOpenAbout = (ImageView) findViewById(R.id.main_craftsman_logo);
        imgOpenAbout.setOnClickListener(this);

        mLeDeviceListAdapter = new DeviceListAdapter(this, savedDevices);
        lstDevice = (ListView) findViewById(R.id.device_list);
        lstDevice.setAdapter(mLeDeviceListAdapter);


    }

    private void initGlobalDevices() {
        Gson gson = new Gson();
        String json = SpHelper.getSharedPreferenceString(this, SpHelper.DEVICE_KEY, null);

        if (!Global.savedDevices.isEmpty()) {
            Global.savedDevices.clear();
        }

        try {
            Global.savedDevices.addAll(Arrays.asList(gson.fromJson(json, DeviceStorage[].class)));

            for (int i = 0; i < Global.savedDevices.size(); i++) {
                Global.connectionQueue.add(savedDevices.get(i));
                Global.savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
            }

        } catch (Exception e) {
            Log.i(TAG, "SharedPreferences are empty!");
            Global.savedDevices = new ArrayList<>();
        }
    }

    private void checkPermissions() {
        Permission permissions = new Permission(this);
        if (!permissions.isBluetoothSupported(mBluetoothAdapter)) {
            finish();
        }
        if (!permissions.isBLESupported()) {
            finish();
        }
        if (!permissions.isBluetoothEnabled(mBluetoothAdapter)) {
            btnAddNewDevice.setVisibility(GONE);
            layoutAlert.setVisibility(View.VISIBLE);
            lstDevice.setVisibility(GONE);
        } else {
            layoutAlert.setVisibility(GONE);
        }
        permissions.isLocationEnabled(this, mLocationManager);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    if (Build.VERSION.SDK_INT < 21) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    } else {
                        mLEScanner.stopScan(mScanCallback21);

                    }
                }
            }, Constants.SCAN_PERIOD);

            mScanning = true;
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                mLEScanner.startScan(filters, settings, mScanCallback21);
            }


        } else {
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                mLEScanner.stopScan(mScanCallback21);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buy_lock_button:
                openBuyStoreLocation();
                break;
            case R.id.main_craftsman_logo:
                openAboutUs();
                break;
            case R.id.add_new_device:
                openNewDeviceDialog();
                break;
            case R.id.dialog_barcode_scanner:
                openBarCodeScanner();
                break;
            case R.id.button_confirm_new_pin:
                if(auth.isPinValid(etPin.getText().toString())){
                    layoutLogin.setVisibility(GONE);
                    auth.showOtherLayouts(toolbar,layoutDevice);
                }
                break;
        }
    }

    private void openNewDeviceDialog() {

        LayoutInflater inflater = getLayoutInflater();
        dialogLayout = inflater.inflate(R.layout.dialog_add_new_device, null);

        imgDialogConfirm = (ImageView) dialogLayout.findViewById(R.id.dialog_confirm);
        etDeviceName = (EditText) dialogLayout.findViewById(R.id.dialog_add_name);
        etDeviceSerial = (EditText) dialogLayout.findViewById(R.id.dialog_add_serial);


        etDeviceSerial.setOnClickListener(this);
        imgBarcodeScanner = (ImageView) dialogLayout.findViewById(R.id.dialog_barcode_scanner);
        imgBarcodeScanner.setOnClickListener(this);


        etDeviceName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });


        final AlertDialog.Builder dialogAddNewDevice = new AlertDialog.Builder(this);
        dialogAddNewDevice.setCancelable(true);
        dialogAddNewDevice.setView(dialogLayout);

        final AlertDialog dialog = dialogAddNewDevice.create();
        dialog.show();

        etDeviceName.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        mCustomKeyboard = new CustomKeyboard(R.id.keyboard_view, R.layout.hex_keyboard, dialogLayout, dialog);
        mCustomKeyboard.registerEditText(R.id.dialog_add_serial, dialogLayout);

        dialogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCustomKeyboard.isCustomKeyboardVisible())
                    mCustomKeyboard.hideCustomKeyboard();
            }
        });


        if (BuildConfig.DEBUG) {
            // do something for a debug build

            imgDialogConfirm.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    etDeviceSerial.setText("EC24B811E2CF");
                    return false;
                }
            });

        }

        etDeviceSerial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDeviceSerial.setSelection(etDeviceSerial.getText().length());
            }
        });

        etDeviceSerial.addTextChangedListener(new TextWatcher() {

            int length_before = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                length_before = s.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Validators.formatSerialInput(s, length_before);
            }

        });

        imgDialogConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < savedDevices.size(); i++) {
                    savedDevices.get(i).setAuthenticated(false);
                    savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }

                final String name = etDeviceName.getText().toString();
                final String serial = etDeviceSerial.getText().toString().toUpperCase().replace("-", "").trim();

                final boolean[] hasAlready = {false};

                if (currentDevice != null)
                    currentDevice = null;

                disposeBluetooth();


                // BT scan callback
                if (Validators.validateDeviceData(serial, name, mContext)) {

                    final String serialPart = (serial.substring(serial.length() - 4, serial.length()));

                    if (Build.VERSION.SDK_INT < 21) {
                        newDeviceScanCallback = new BluetoothAdapter.LeScanCallback

                                () {

                            @Override
                            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                                validateNewDevice(device, serialPart, serial, name, hasAlready);
                            }
                        };
                    }


                    if (Build.VERSION.SDK_INT >= 21) {
                        newDeviceScanCallback21 = new ScanCallback() {
                            @Override
                            public void onScanResult(int callbackType, ScanResult result) {
                                validateNewDevice(result.getDevice(), serialPart, serial, name, hasAlready);
                            }

                            @Override
                            public void onBatchScanResults(List<ScanResult> results) {
                                for (ScanResult sr : results) {
                                    Log.i("ScanResult - Results", sr.toString());
                                }
                            }

                            @Override
                            public void onScanFailed(int errorCode) {
                                Log.e("Scan Failed", "Error Code: " + errorCode);
                            }
                        };

                    }


                    // Add new device async
                    new AsyncTask<Void, Void, String>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            mScanning = true;
                            btnAddNewDevice.setVisibility(GONE);
                            layoutConnecting.setVisibility(View.VISIBLE);
                            lstDevice.setVisibility(GONE);

                            if (Build.VERSION.SDK_INT < 21) {
                                mBluetoothAdapter.startLeScan(newDeviceScanCallback);
                            } else {
                                mLEScanner.startScan(filters, settings, newDeviceScanCallback21);
                            }
                        }


                        @Override
                        protected String doInBackground(Void... params) {
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mScanning = false;
                                    //mBluetoothAdapter.stopLeScan(newDeviceScanCallback);

                                    if (Build.VERSION.SDK_INT < 21) {
                                        mBluetoothAdapter.stopLeScan(newDeviceScanCallback);
                                    } else {
                                        mLEScanner.stopScan(newDeviceScanCallback21);
                                    }

                                    if (hasAlready[0]) {
                                        showDeviceAlert("There is a device with that serial number");

                                    } else {

                                        if (currentDevice == null) {
                                            showDeviceAlert("There is no device with that serial number.");
                                        }

                                    }

                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }

                                }
                            }, Constants.SCAN_PERIOD);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);


                        }

                    }.execute();

                    dialog.dismiss();
                }

            }

        });

    }

    private void showDeviceAlert(String message) {
        new AlertDialog.Builder(mContext)
                .setTitle("Error")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                })
                .create()
                .show();
        changeConnectingView(GONE);
    }

    private void validateNewDevice(BluetoothDevice device, String serialPart, String serial, String name, boolean[]
            hasAlready) {
        if (device.getName() != null) {

            if (Validators.validateNewAddedDevice(device.getName(), serialPart)) {
                if (!Validators.containSerial(savedDevices, serial)) {
                    currentDevice = new DeviceStorage(name, serial, device);
                    mBluetoothLeService.connect(device.getAddress());
                } else {
                    hasAlready[0] = true;
                }
            }
        }
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void openBarCodeScanner() {
        IntentIntegrator scanIntegrator = new IntentIntegrator((Activity) mContext);
        scanIntegrator.initiateScan();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            checkPermissions();
        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<ScanFilter>();
            }
            scanLeDevice(true);
        }

        if(SpHelper.getSharedPreferenceBoolean(this, SpHelper.USER_AUTH, false)) {
            etPin.getText().clear();
            layoutLogin.setVisibility(View.VISIBLE);
            auth.hideOtherLayouts(toolbar,layoutDevice);
            setKeyboardDelay(etPin);
            etPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        btnConfirmPin.performClick();
                    }
                    return false;
                }
            });
        }


        initConnectionService();
        registerReceiver(mGattUpdateReceiver, Constants.makeGattUpdateIntentFilter());

        mLeDeviceListAdapter.notifyDataSetChanged();
    }

    private void setKeyboardDelay(final EditText et) {
        etPin.postDelayed(new Runnable() {
            @Override
            public void run() {
                showKeyboard(et);
            }
        },50);
    }


    @Override
    protected void onPause() {
        super.onPause();
//        if (mGattUpdateReceiver != null) {
//            unregisterReceiver(mGattUpdateReceiver);
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

//        if (mServiceConnection != null) {
//            unbindService(mServiceConnection);
//        }
//
//        if (mBluetoothLeService != null) {
//            mBluetoothLeService = null;
//        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        //registerReceiver(btReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        if (btReceiver != null) {
//            unregisterReceiver(btReceiver);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            Boast.makeText(this, R.string.required_bluetooth, Toast.LENGTH_SHORT).show();
        }

        if (resultCode == RESULT_OK && requestCode != REQUEST_ENABLE_BT) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanResult != null) {

                String contents = scanResult.getContents();
                etDeviceSerial.setText(contents);

            } else {
                Log.e(TAG, "Barcode scan unsuccessful");
                Toast.makeText(mContext, "The code is not scanned successfully!\nPlease try again.", Toast
                        .LENGTH_SHORT)
                        .show();
            }
        }

        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void openBuyStoreLocation() {
        WebHelper webSite = new WebHelper();
        webSite.goToBuyStore(this, Uri.parse(Constants.getWebUrl()));
    }

    private void openAboutUs() {
        if (BuildConfig.FLAVOR.equals(Constants.craftsmanFlavour)) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
        }
    }

    private void initBTReceiver() {
        filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);

        btReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();

                if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);


                    switch (state) {

                        case BluetoothAdapter.STATE_OFF:
                            Log.i(TAG, "Bluetooth off");

                            btnAddNewDevice.setVisibility(GONE);
                            layoutAlert.setVisibility(View.VISIBLE);
                            lstDevice.setVisibility(GONE);
                            break;

                        case BluetoothAdapter.STATE_TURNING_OFF:
                            Log.i(TAG, "Turning Bluetooth off...");

                            disposeBluetooth();
                            if (currentDevice != null) {
                                currentDevice = null;
                            }


                            mLeDeviceListAdapter.clear();

                            initGlobalDevices();
                            mLeDeviceListAdapter.notifyDataSetChanged();

                            lstDevice.setVisibility(GONE);


                            break;

                        case BluetoothAdapter.STATE_ON:
                            Log.i(TAG, "Bluetooth on");

                            btnAddNewDevice.setVisibility(View.VISIBLE);
                            layoutAlert.setVisibility(GONE);
                            lstDevice.setVisibility(View.VISIBLE);

                            initConnectionService();
                            registerReceiver(mGattUpdateReceiver, Constants.makeGattUpdateIntentFilter());



                            if (mBluetoothLeService != null) {

                                for (int i = 0; i < savedDevices.size(); i++) {
                                    // TODO: 11/4/2016  current device nece ici ovako
                                    savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
                                    savedDevices.get(i).setFirstRun(true);
                                    savedDevices.get(i).setHardware(null);
                                    savedDevices.get(i).setFirmware(null);
                                    savedDevices.get(i).setAuthenticated(false);
//                                    currentDevice = savedDevices.get(i);
//                                    boolean result = mBluetoothLeService.connect(savedDevices.get(i).getMacAddress());
//                                    Log.d(TAG, "Connect request result=" + result + " " + savedDevices.get(i)
//                                            .getMacAddress());
                                }
                                mLeDeviceListAdapter.notifyDataSetChanged();
                            }

                            if (Build.VERSION.SDK_INT >= 21) {
                                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                                settings = new ScanSettings.Builder()
                                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                                        .build();
                                filters = new ArrayList<ScanFilter>();
                                getNewScanCallback();
                            }

                            // scanLeDevice(true);


                            break;


                        case BluetoothAdapter.STATE_TURNING_ON:
                            Log.i(TAG, "Turning Bluetooth on");
                            for (int i = 0; i < savedDevices.size(); i++) {
                                savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.IN_PROGRESS);
//                                savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
                            }
                            break;
                    }
                }
            }
        };
    }

    private void disposeBluetooth() {

        mBluetoothLeService.disconnect();
        mBluetoothLeService.close();
        mBluetoothLeService.stopSelf();

        if (!savedDevices.isEmpty()) {
            for (int i = 0; i < savedDevices.size(); i++) {
                savedDevices.get(i).setFirstRun(true);
                savedDevices.get(i).setHardware(null);
                savedDevices.get(i).setFirmware(null);
                savedDevices.get(i).setAuthenticated(false);
            }
        }

    }

    @Override
    public void onClicked(String deviceAddress) {
        Log.i(TAG, "List item is clicked!");

        if (currentDevice != null) {
            currentDevice = null;
        }



        currentDevice = getDeviceByAddress(deviceAddress);

        mBluetoothLeService.connect(deviceAddress);

        if (currentDevice == null)
            return;

        if(!currentDevice.getDeviceStatus().equals(DeviceStorage.DeviceStatus.AUTH)) {

            for (int i = 0; i < savedDevices.size(); i++) {
                if (savedDevices.get(i).getMacAddress().equals(deviceAddress)) {
                    savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.IN_PROGRESS);

                    //          break;
                } else {
                    savedDevices.get(i).set_tempStatus(savedDevices.get(i).getDeviceStatus());
                    savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.WAIT);
//                savedDevices.get(i).setFirstRun(true);
//                savedDevices.get(i).setHardware(null);
//                savedDevices.get(i).setFirmware(null);
//                savedDevices.get(i).setAuthenticated(false);
                }
            }
        }else{
            currentDevice.setDeviceStatus(DeviceStorage.DeviceStatus.IN_PROGRESS);
            gattReceiverHelper.updateDeviceState(currentDevice.getMacAddress(),savedDevices,DeviceStorage
                    .DeviceStatus.IN_PROGRESS);


        }
        mLeDeviceListAdapter.notifyDataSetChanged();

        if (mScanning) {
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                mLEScanner.stopScan(mScanCallback21);
            }
            mScanning = false;
        }

        if (!currentDevice.isAuthenticated()) {
            mBluetoothLeService.connect(currentDevice.getMacAddress());
        } else {
            //lock & unlock
            //mBluetoothLeService.writeCharacteristic(Constants.LOCK_OR_UNLOCK, LS_LOCK, LC_UNLOCK);
            gattReceiverHelper.lockOrUnlock(mBluetoothLeService);
        }

    }

    private DeviceStorage getDeviceByAddress(String deviceAddress) {
        return Validators.getDeviceByAddress(savedDevices, deviceAddress);
    }

    private void askForPermission() {
        int PERMISSION_LOCATION_ID = 1352;
        permission.askForLocationRuntimePermission(PERMISSION_LOCATION_ID);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        permission.askForLocationRuntimePermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        permission.exitPermission(builder, this);
    }

    @Override
    public void onRename(final String deviceAddress) {
        LayoutInflater inflater = getLayoutInflater();
        dialogLayout = inflater.inflate(R.layout.dialog_rename_device, null);

        imgDialogConfirm = (ImageView) dialogLayout.findViewById(R.id.dialog_confirm);
        etDeviceName = (EditText) dialogLayout.findViewById(R.id.dialog_add_name);

        final AlertDialog.Builder dialogAddNewDevice = new AlertDialog.Builder(this);
        permission.renameLock(dialogAddNewDevice, dialogLayout, etDeviceName, deviceAddress, imgDialogConfirm,
                mLeDeviceListAdapter);
    }

    @Override
    public void onRemove(final String deviceAddress) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.SwipeDialogLight);
        permission.removeLock(builder, deviceAddress, mLeDeviceListAdapter);
    }


}