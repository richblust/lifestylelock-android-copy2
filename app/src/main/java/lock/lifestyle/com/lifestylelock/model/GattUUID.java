package lock.lifestyle.com.lifestylelock.model;

/**
 * Created by QSD on 9/26/2016.
 */


/**
 * This class includes a subset of GATT attributes
 */

public class GattUUID {


    /** List of UUIDs
     *
     LS - Lock service
     LC - Lock characteristic
     LD - Lock descriptor
     *
     **/

    // Lock service
    public static final String LS_LOCK = "24561a10-6b91-11e4-9803-0800200c9a66";
    // Lock characteristics
    public static final String LC_AUTHENTICATION_CHALLENGE = "401be8b0-b231-11e4-ab27-0800200c9a66";
    public static final String LC_AUTHENTICATION_HASH = "8b9fc490-b232-11e4-ab27-0800200c9a66";
    public static final String LC_UNLOCK = "d82d87d0-6b91-11e4-9803-0800200c9a66";
    public static final String LC_LOCK_QUERY = "df4db800-6b91-11e4-9803-0800200c9a66";
    public static final String LC_TAMPER_SENSE = "e4efaca0-6b91-11e4-9803-0800200c9a66";
    public static final String LC_BATTERY_COVER = "01f1c76d-fdab-4a9d-86ce-8a30d294a71d";
    public static final String LC_LOCK_LOG = "09870f09-a875-446f-8594-6a2c9bda1b1a";

    // Time service
    public static final String LS_TIME_SYNC = "64e9837d-ca48-48c4-bb71-323e5a85b51f";
    // Time characteristics
    public static final String LC_TIME_SYNC = "51686b01-7b48-4675-891c-ddc7c4b36e66";
    public static final String LC_TIME_ZONE_OFFSET = "e0c37fd1-def8-44b0-a3db-ea9bad6f4d4e";
    public static final String LC_LOCK_ON_DISCONNECT = "f4c662e1-9a55-4899-8d13-b04255655b73";

    // Configuration service
    public static final String LS_CONFIGURATION = "8b5f5e40-1cf0-11e5-9a21-1697f925ec7b";
    // Configuration characteristics
    public static final String LC_RAPID_ACCESS = "8b5f641c-1cf0-11e5-9a21-1697f925ec7b";
    public static final String LC_TAMPER_SENSE_SENSITIVITY = "8b5f65de-1cf0-11e5-9a21-1697f925ec7b";


    // Update service
    public static final String LS_UPDATE = "25a22330-820f-11e3-baa7-0800200c9a66";
    // Update characteristics
    public static final String LC_UPDATE_SERVICE_CONTROL = "47c17590-a9c1-11e3-a5e2-0800200c9a66";
    public static final String LC_UPDATE_SERVICE_HASH = "0d6697a8-a13a-4979-b82e-cebf6e7aee1e";
    public static final String LC_UPDATE_SERVICE_CHUNK_SIZE = "4444ef8bb-bb2e-4bdb-8617-4b71abece1a0";
    public static final String LC_UPDATE_SERVICE_DATA = "5d55d5b0-959c-11e3-a5e2-0800200c9a66";
    public static final String LC_UPDATE_SERVICE_CHUNK_COMPLETE_NOTIFICATION =
            "1fe0485c-7cd8-4ca9-8685-bf1a90c40789";

    // Battery service
    public static final String LS_BATTERY = "0000180f-0000-1000-8000-00805f9b34fb";
    // Battery characteristics
    public static final String LC_BATTERY = "00002a19-0000-1000-8000-00805f9b34fb";
    public static final int LC_PERIPHERAL_NUMBER_OF_BATTERY = 2;
    public static final String LOCK_PERIPHERAL_BATTERY_VDD_KEY = "VDD";
    public static final String LOCK_PERIPHERAL_BATTERY_EXT_1_KEY = "EXT1";


    // Device service
    public static final String LS_DEVICE_INFORMATION = "0000180a-0000-1000-8000-00805f9b34fb";
    // Device characteristics
    public static final String LC_FIRMWARE = "00002a26-0000-1000-8000-00805f9b34fb";
    public static final String LC_HARDWARE_VERSION = "00002a27-0000-1000-8000-00805f9b34fb";


    // Descriptors

    public static final String LD_DESCRIPTOR = "00002902-0000-1000-8000-00805f9b34fb";




}
