package lock.lifestyle.com.lifestylelock.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import java.util.UUID;

import lock.lifestyle.com.lifestylelock.model.DeviceStorage;
import lock.lifestyle.com.lifestylelock.model.Global;

import static lock.lifestyle.com.lifestylelock.model.Global.savedDevices;

/**
 * Created by QSD on 10/12/2016.
 */

public class CharacteristicHelper {


    private static final String TAG = "Characteristics";


    public static void writeCustomCharacteristic(byte[] value, BluetoothAdapter mBluetoothAdapter, BluetoothGatt
            mBluetoothGatt, String serviceUUID, String characteristicUUID) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        BluetoothGattService mCustomService = mBluetoothGatt.getService(UUID.fromString(serviceUUID));
        if (mCustomService == null) {
            Log.w(TAG, "Write - Custom BLE Service not found "+serviceUUID);

            for (int i = 0; i < Global.savedDevices.size(); i++) {
                if(Global.savedDevices.get(i).getMacAddress().equals(mBluetoothGatt.getDevice().getAddress())){
                    savedDevices.get(i).setDeviceStatus(DeviceStorage.DeviceStatus.AUTH);
                    savedDevices.get(i).setFirstRun(true);
                    savedDevices.get(i).setHardware(null);
                    savedDevices.get(i).setFirmware(null);
                    savedDevices.get(i).setAuthenticated(false);
                }

            }


            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(UUID.fromString
                (characteristicUUID));
        //mWriteCharacteristic.setValue(value,android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT8,0);
        mWriteCharacteristic.setValue(value);

        if (!mBluetoothGatt.writeCharacteristic(mWriteCharacteristic)) {
            Log.w(TAG, "Failed to write characteristic");
        }
    }

    public static void readCustomCharacteristic(BluetoothAdapter mBluetoothAdapter, BluetoothGatt mBluetoothGatt,
                                                String serviceUUID, String characteristicUUID) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        BluetoothGattService mCustomService = mBluetoothGatt.getService(UUID.fromString(serviceUUID));
        if (mCustomService == null) {
            Log.w(TAG, "Read - Custom BLE Service not found");
            mBluetoothGatt.connect();

            return;
        }
        /*get the read characteristic from the service*/
        BluetoothGattCharacteristic mReadCharacteristic = mCustomService.getCharacteristic(UUID.fromString
                (characteristicUUID));
        //byte[] data = mReadCharacteristic.getValue();

        if (!mBluetoothGatt.readCharacteristic(mReadCharacteristic)) {
            Log.w(TAG, "Failed to read characteristic");
        }
    }





}
