package lock.lifestyle.com.lifestylelock.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.storage.SpHelper;

public class SplashActivity extends AppCompatActivity implements Runnable {

    private final int splashTime = 1000;
    TextView txtSplash;
    boolean isUserAuth;
    boolean isFirstTimeRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();

        isFirstTimeRun = SpHelper.getSharedPreferenceBoolean(this,SpHelper.FIRST_TIME,true);

        isUserAuth = SpHelper.getSharedPreferenceBoolean(this,SpHelper.USER_AUTH,false);

        new Handler().postDelayed(this, splashTime);

    }

    @Override
    public void run() {
        if(!isFirstTimeRun) {
            if (isUserAuth) {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(this, AuthActivity.class);
                startActivity(intent);
                finish();
            }
        }else{
            Intent intent = new Intent(this, TermsOfServices.class);
            startActivity(intent);
            finish();
        }

    }

    private void init() {
        txtSplash = (TextView) findViewById(R.id.splash_text);
    }

}
