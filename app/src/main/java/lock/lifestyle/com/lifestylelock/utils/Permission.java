package lock.lifestyle.com.lifestylelock.utils;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.activities.MainActivity;
import lock.lifestyle.com.lifestylelock.adapters.DeviceListAdapter;
import lock.lifestyle.com.lifestylelock.storage.SpHelper;

import static lock.lifestyle.com.lifestylelock.model.Global.savedDevices;

/**
 * Created by QSD on 19/Oct/2016.
 * Class is used for android permissions
 */

public class Permission {

    private final static String TAG = Permission.class.getSimpleName();

    private Context context;

    public Permission(Context mContext) {
        context = mContext;
    }

    // Checks if Bluetooth is supported on the device.
    public boolean isBluetoothSupported(BluetoothAdapter mBluetoothAdapter) {

        if (mBluetoothAdapter == null) {
            Boast.makeText(context, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    // Use this check to determine whether BLE is supported on the device.
    public boolean isBLESupported() {
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Boast.makeText(context, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    // Checks if Bluetooth is enabled
    public boolean isBluetoothEnabled(BluetoothAdapter mBluetoothAdapter) {
        if (!mBluetoothAdapter.isEnabled()) {
            return false;
        } else {
            return true;
        }
    }


    // Checks if Location is enabled
    public void isLocationEnabled(final Context context, LocationManager mLocationManager) {

        if (Build.VERSION.SDK_INT >= 23) {

            boolean isGpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


            if (!isGpsEnabled) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setMessage("Please enable location manager");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(myIntent);
                        //get gps
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        ((Activity) context).finish();
                        Boast.makeText(context, "Application can't work if location is not enabled", Toast.LENGTH_LONG)
                                .show();

                    }
                });
                dialog.show();
            }
        }
    }

    // Ask for location permission in runtime
    public void askForLocationRuntimePermission(int PERMISSION_LOCATION_ID) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ((Activity) context).requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_LOCATION_ID);
            }
        }
    }


    // OnActivityResult for location permission
    public void askForLocationRuntimePermissionResult(int requestCode, String permissions[], int[] grantResults) {

        // PERMISSION_ID permission location id in MainActivity
        final int PERMISSION_ID = 1352;
        switch (requestCode) {
            case PERMISSION_ID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission is granted");
                } else {
                    Boast.makeText(context, "This application requires permissions to work properly", Toast
                            .LENGTH_LONG).show();
                    ((Activity) context).finish();

                }
            }
        }
    }

    // onBackPress exit permission
    public void exitPermission(AlertDialog.Builder builder, final Context context) {
        builder.setMessage(context.getString(R.string.dialog_exit_popup))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) context).finish();
                    }
                })
                .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // Permission to remove lock from list
    public void removeLock(AlertDialog.Builder builder, final String deviceAddress, final DeviceListAdapter
            mLeDeviceListAdapter) {
        builder.setTitle("Remove lock?");
        builder.setMessage("Are you sure you want to remove this lock? You will need its serial number to add it " +
                "again.");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                int pos = -1;
                for (int j = 0; j < savedDevices.size(); j++) {
                    if (savedDevices.get(j).getMacAddress().equals(deviceAddress)) {
                        pos = j;
                        break;
                    }
                }

                if (pos != -1) {
                    savedDevices.remove(pos);
                    SpHelper.setSharedPreferenceString(context, SpHelper.DEVICE_KEY, new Gson().toJson(savedDevices));
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }

            }
        });
        builder.setNegativeButton("Keep", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                // do nothing
            }
        });

        builder.create().show();
    }

    // Permission to rename lock from list
    public void renameLock(AlertDialog.Builder dialogAddNewDevice, View dialogLayout, final EditText etDeviceName,
                           final String deviceAddress, ImageView imgDialogConfirm, final DeviceListAdapter
                                   mLeDeviceListAdapter) {


        dialogAddNewDevice.setCancelable(true);
        dialogAddNewDevice.setView(dialogLayout);

        final AlertDialog dialog = dialogAddNewDevice.create();
        dialog.show();

        etDeviceName.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);



        imgDialogConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < savedDevices.size(); i++) {
                    if (savedDevices.get(i).getMacAddress().equals(deviceAddress)) {
                        savedDevices.get(i).setName(etDeviceName.getText().toString());
                        SpHelper.setSharedPreferenceString(context, SpHelper.DEVICE_KEY, new Gson().toJson
                                (savedDevices));
                        mLeDeviceListAdapter.notifyDataSetChanged();
                        break;
                    }
                }

                dialog.dismiss();
            }
        });
    }
}



