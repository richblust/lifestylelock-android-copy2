package lock.lifestyle.com.lifestylelock.model;

import android.bluetooth.BluetoothDevice;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import lock.lifestyle.com.lifestylelock.utils.Converter;
import lock.lifestyle.com.lifestylelock.utils.HashComponents;

/**
 * Created by QSD on 24/Oct/2016.
 * Class is used for
 */

public class DeviceStorage {

    @SerializedName ("name")
    private String mName;
    @SerializedName ("deviceName")
    private String mDeviceName;
    @SerializedName ("serial")
    private String mSerial;
    @SerializedName ("address")
    private String mMacAddress;
    @SerializedName ("uuid")
    private String mUUID;
    @SerializedName ("serialPart")
    private String mSerialPart;

    @SerializedName ("clientChallenge")
    private byte[] mClientChallenge;

    @SerializedName ("serverChallenge")
    private byte[] mServerChallenge;

    @SerializedName ("serialByte")
    private byte[] serialByte;

    @SerializedName ("hash")
    private byte[] hash;

    @SerializedName ("encryptedHash")
    private byte[] encryptedHash;

    @SerializedName ("deviceStatus")
    private DeviceStatus deviceStatus;

    @SerializedName ("deviceLockState")
    private byte[] deviceLockState;

    private long currentTime;

    //    @SerializedName ("mFirmware")
    private transient String mFirmware;

    //    @SerializedName ("hardwareVersion")
    private transient String mHardware;


    private transient boolean isAuthenticated;
    private transient BluetoothDevice stockBluetoothDevice;
    private transient byte[] batteryLevel;

    private transient boolean isFirstRun = true;
    private transient boolean isSubscribed = false;

    public DeviceStatus get_tempStatus() {
        return _tempStatus;
    }

    public void set_tempStatus(DeviceStatus _tempStatus) {
        this._tempStatus = _tempStatus;
    }

    private transient DeviceStatus _tempStatus;



    public DeviceStorage() {
    }

    public DeviceStorage(String name, String serial, BluetoothDevice device) {
        mName = name;
        mDeviceName = device.getName();
        mMacAddress = device.getAddress();
        mSerial = serial.toUpperCase();
        serialByte = Converter.hexStringToByteArray(serial.toUpperCase());
        mUUID = UUID.randomUUID().toString();
        mSerialPart = (serial.substring(serial.length() - 4, serial.length()));
        mClientChallenge = HashComponents.generateClientChallenge();
        isAuthenticated = false;
        isFirstRun = true;
        isSubscribed = false;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSerial() {
        return mSerial;
    }

    public void setSerial(String serial) {
        mSerial = serial;
    }

    public String getMacAddress() {
        return mMacAddress;
    }

    public void setMacAddress(String macAddress) {
        mMacAddress = macAddress;
    }

    public String getUUID() {
        return mUUID.toString();
    }

    public String getSerialPart() {
        return mSerialPart;
    }

    public String getDeviceName() {
        return mDeviceName;
    }

    public void setDeviceName(String deviceName) {
        mDeviceName = deviceName;
    }

    public byte[] getClientChallenge() {
        return mClientChallenge;
    }

    public byte[] getServerChallenge() {
        return mServerChallenge;
    }

    // Auth get / set

    public void setServerChallenge(byte[] serverChallenge) {
        mServerChallenge = serverChallenge;
    }

    public byte[] getSerialByte() {
        return serialByte;
    }

    public void setSerialByte(byte[] serialByte) {
        this.serialByte = serialByte;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    public byte[] getEncryptedHash() {
        return encryptedHash;
    }

    public void setEncryptedHash(byte[] encryptedHash) {
        this.encryptedHash = encryptedHash;
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        isAuthenticated = authenticated;
    }

    public byte[] getDeviceLockState() {
        return deviceLockState;
    }

    public void setDeviceLockState(byte[] deviceLockState) {
        this.deviceLockState = deviceLockState;
    }

    public BluetoothDevice getStockBluetoothDevice() {
        return stockBluetoothDevice;
    }

    public void setStockBluetoothDevice(BluetoothDevice stockBluetoothDevice) {
        this.stockBluetoothDevice = stockBluetoothDevice;
    }

    public byte[] getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(byte[] batteryLevel) {
        this.batteryLevel = batteryLevel;
    }


    public String getFirmware() {
        return mFirmware;
    }

    public void setFirmware(String firmware) {
        this.mFirmware = firmware;
    }

    public String getHardware() {
        return mHardware;
    }

    public void setHardware(String hardware) {
        this.mHardware = hardware;
    }

    public DeviceStatus getDeviceStatus() {
        if (deviceStatus == null) {
            deviceStatus = DeviceStatus.AUTH;
        }
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public boolean isFirstRun() {
        return isFirstRun;
    }

    public void setFirstRun(boolean firstRun) {
        isFirstRun = firstRun;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    // Lock states
    public enum DeviceStatus {
        LOCKED, IN_PROGRESS, UNLOCKED, AUTH,WAIT
    }
}
