package lock.lifestyle.com.lifestylelock.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.utils.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TermsOfUseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFont();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_use);

        WebView webTerms;
        webTerms = (WebView) findViewById(R.id.terms_web_view);
        webTerms.loadUrl("file:///android_asset/TermsOfUseAbout.html");
        //same as background color space_color
        webTerms.setBackgroundColor(Color.parseColor("#211f20"));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath(Constants.getFontName())
                .setFontAttrId(R.attr.fontPath).build());
    }

}
