package lock.lifestyle.com.lifestylelock.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import lock.lifestyle.com.lifestylelock.R;
import lock.lifestyle.com.lifestylelock.model.Authentication;
import lock.lifestyle.com.lifestylelock.utils.Boast;
import lock.lifestyle.com.lifestylelock.utils.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.view.View.GONE;

public class AuthActivity extends AppCompatActivity implements View.OnClickListener{
    EditText etPin,etPinConfirm;
    Button btnConfirm,btnSkip;
    Authentication auth;
    boolean isPasswordUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFont();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isPasswordUpdate = extras.getBoolean("is_password_update");
        }

        init();

    }


    private void init(){


        etPin = (EditText) findViewById(R.id.et_new_pin);
        etPin.setOnClickListener(this);
        etPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        etPin.requestFocus();




        etPinConfirm = (EditText) findViewById(R.id.et_new_pin_confirm);
        etPinConfirm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }else{
                    showKeyboard(etPinConfirm);
                }
            }
        });

        btnConfirm = (Button) findViewById(R.id.button_confirm_new_pin);
        btnConfirm.setOnClickListener(this);
        btnSkip = (Button) findViewById(R.id.button_skip);
        btnSkip.setOnClickListener(this);

        if(isPasswordUpdate){
            btnSkip.setVisibility(GONE);
            etPin.setHint("New PIN");

            etPinConfirm.setHint("PIN confirm");
        }


        setKeyboardDelay(etPin);

        etPinConfirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    btnConfirm.performClick();
                }

                return false;
            }
        });


        auth = new Authentication(this);
    }




    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_confirm_new_pin:

                if(auth.addNewPin(etPin.getText().toString(),etPinConfirm.getText().toString())){
                    if(!isPasswordUpdate) {
                        Intent intent = new Intent(AuthActivity.this, MainActivity.class);
                        intent.putExtra("is_first", true);
                        startActivity(intent);
                    }
                    finish();
                }
                break;

            case R.id.button_skip:
                auth.onSkipPress();
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath(Constants.getFontName())
                .setFontAttrId(R.attr.fontPath).build());
    }

    private void setKeyboardDelay(final EditText et) {
        etPin.postDelayed(new Runnable() {
            @Override
            public void run() {
                showKeyboard(et);
            }
        },50);
    }



}
