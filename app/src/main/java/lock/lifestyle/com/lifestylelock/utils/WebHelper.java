package lock.lifestyle.com.lifestylelock.utils;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;

import lock.lifestyle.com.lifestylelock.R;

/**
 * Created by QSD on 19/Oct/2016.
 * Class is used for connection sto web store portal using custom tab intent
 */

public class WebHelper {

    public WebHelper() {
    }

    public void goToBuyStore(Context context, Uri url) {
        CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
        intentBuilder.setToolbarColor(ContextCompat.getColor(context, R.color.browser_color));

        // Define entry and exit animation
        intentBuilder.setExitAnimations(context, R.anim.right_to_left_end, R.anim.left_to_right_end);
        intentBuilder.setStartAnimations(context, R.anim.left_to_right_start, R.anim.right_to_left_start);
        intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(context, R.color.browser_color));
        CustomTabsIntent customTabsIntent = intentBuilder.build();
        customTabsIntent.launchUrl((Activity) context, url);
    }
}
