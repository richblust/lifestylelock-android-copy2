package lock.lifestyle.com.lifestylelock.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import lock.lifestyle.com.lifestylelock.model.DeviceStorage;
import lock.lifestyle.com.lifestylelock.services.BluetoothLeService;

import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_UNLOCK;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LS_LOCK;
import static lock.lifestyle.com.lifestylelock.model.Global.currentDevice;

/**
 * Created by adnan on 20.10.2016..
 */

public class GattReceiverHelper {

    // Update device status ( Circle image, lock image, text )
    public void lockStateUpdater(String address, List<DeviceStorage> devices) {


        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_LOCKED)) {
            updateDeviceState(address, devices, DeviceStorage.DeviceStatus.LOCKED);
        }

        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_TIME_UNLOCK_CLOSED)) {
            updateDeviceState(address, devices, DeviceStorage.DeviceStatus.LOCKED);
        }

        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_UNLOCKED_AUTOMATIC_LOCK)) {
            updateDeviceState(address, devices, DeviceStorage.DeviceStatus.LOCKED);
        }

        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_UNLOCKED_MANUAL_LOCK)) {
            updateDeviceState(address, devices, DeviceStorage.DeviceStatus.UNLOCKED);
        }
    }

    public void updateDeviceState(String address, List<DeviceStorage> devices, DeviceStorage.DeviceStatus
            deviceStatus) {
        for (int i = 0; i < devices.size(); i++) {
            if (devices.get(i).getMacAddress().equals(address)) {
                devices.get(i).setDeviceStatus(deviceStatus);
                currentDevice.setDeviceStatus(deviceStatus);
                break;
            }
        }
    }


    // Lock or unlock device depends from device state
    public void lockOrUnlock(BluetoothLeService mBluetoothLeService) {


        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_LOCKED)) {

            mBluetoothLeService.writeCharacteristic(Constants.LOCK_OR_UNLOCK, LS_LOCK, LC_UNLOCK);
        }

        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_TIME_UNLOCK_CLOSED)) {

            mBluetoothLeService.writeCharacteristic(Constants.LOCK_OR_UNLOCK, LS_LOCK, LC_UNLOCK);
        }

        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_UNLOCKED_AUTOMATIC_LOCK)) {

            mBluetoothLeService.writeCharacteristic(Constants.LOCK_OR_UNLOCK, LS_LOCK, LC_UNLOCK);
        }

        if (Arrays.equals(currentDevice.getDeviceLockState(), Constants.LOCK_STATE_UNLOCKED_MANUAL_LOCK)) {

            //mBluetoothLeService.readCharacteristic(LS_LOCK, LC_LOCK_QUERY);
            mBluetoothLeService.writeCharacteristic(Constants.LOCK_OR_UNLOCK, LS_LOCK, LC_UNLOCK);

        }
    }


    // TODO: 11/15/2016  handle responses


    public void handleAuthResponse(byte[] response, Context context) {

        byte[] authentication_failed = BigInteger.valueOf(0x82).toByteArray();
        byte[] incorrect_length = BigInteger.valueOf(0x83).toByteArray();
        byte[] no_challenge = BigInteger.valueOf(0x84).toByteArray();
        byte[] authentication_limit_reached = BigInteger.valueOf(0x85).toByteArray();
        byte[] invalid_value = BigInteger.valueOf(0x87).toByteArray();

        String message = null;
        String title = null;

        if (Arrays.equals(response, authentication_failed)) {
            message = "Authentication with the peripheral failed. Try again.";
        }

        if (Arrays.equals(response, incorrect_length)) {
            message = "The data written to the GATT characteristic is not the correct length.";
        }

        if (Arrays.equals(response, no_challenge)) {
            message = "No client challenge is provided before writing the client hash.";
        }

        if (Arrays.equals(response, authentication_limit_reached)) {
            message = "The maximum number of failed authentications has been reached.\nWait 5minutes and try again.";
        }

        if (Arrays.equals(response, invalid_value)) {
            message = "An invalid value is written to the GATT characteristic.";
        }


        if (message != null && title != null) {

            new AlertDialog.Builder(context)
                    .setTitle("Error")
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }

                    })
                    .create()
                    .show();


        }


    }


}
