package lock.lifestyle.com.lifestylelock.utils;

import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import lock.lifestyle.com.lifestylelock.BuildConfig;

/**
 * Created by nick on 15/10/2016.
 */

public class HashComponents {
    public final byte[] clientChallenge;
    public final byte[] serverChallenge;
    public final byte[] serial;

    private static String TRANSFORMATION = "AES/CBC/NoPadding";

    private static SecretKeySpec key;
    private static AlgorithmParameterSpec spec;

    static {
        byte[] keyBytes = hexStringToByteArray(BuildConfig.LOCK_KEY);
        key = new SecretKeySpec(keyBytes, "AES");

        byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
        spec = new IvParameterSpec(iv);
    }

    public static byte[] generateClientChallenge() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[2];
        random.nextBytes(bytes);
        return bytes;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static byte[] toBytes(int... ints) { // helper function
        byte[] result = new byte[ints.length];
        for (int i = 0; i < ints.length; i++) {
            result[i] = (byte) ints[i];
        }
        return result;
    }

    public HashComponents(byte[] clientChallenge, byte[] serverChallenge, byte[] serial) throws IllegalArgumentException {
        if(clientChallenge.length != 2) {
            throw new IllegalArgumentException("Invalid client challenge length: must be 2");
        }
        if(serverChallenge.length != 2) {
            throw new IllegalArgumentException("Invalid server challenge length: must be 2");
        }
        if(serial.length != 6) {
            throw new IllegalArgumentException("Invalid serial length: must be 6");
        }
        this.clientChallenge = clientChallenge;
        this.serverChallenge = serverChallenge;
        this.serial = serial;
    }

    public HashComponents(byte[] hashData, boolean isClientHash) throws IllegalArgumentException {
        if(hashData.length != 16) {
            throw new IllegalArgumentException("Invalid hash data length: must be 16");
        }
        byte[] first = new byte[2];
        byte[] second = new byte[2];
        serial = new byte[6];
        System.arraycopy(hashData,0,first,0,2);
        System.arraycopy(hashData,2,second,0,2);
        System.arraycopy(hashData,4,serial,0,6);
        clientChallenge = isClientHash ? first : second;
        serverChallenge = isClientHash ? second : first;
    }

    public byte[] toHashData(boolean isClientHash) {
        byte[] r = new byte[16];
        byte[] first = isClientHash ? clientChallenge : serverChallenge;
        byte[] second = isClientHash ? serverChallenge : clientChallenge;
        System.arraycopy(first, 0, r, 0, first.length);
        System.arraycopy(second, 0, r, 2, second.length);
        System.arraycopy(serial, 0, r, 4, serial.length);
        return r;
    }

    public byte[] encrypt(boolean isClientHash) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, key, spec);
            return cipher.doFinal(this.toHashData(isClientHash));
        } catch (Exception e) {
            return null;
        }
    }

    public static HashComponents decrypt(byte[] encryptedHashData, boolean isClientHash) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, key, spec);
            byte[] decrypted = cipher.doFinal(encryptedHashData);
            return new HashComponents(decrypted, isClientHash);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HashComponents that = (HashComponents) o;

        if (!Arrays.equals(clientChallenge, that.clientChallenge)) return false;
        if (!Arrays.equals(serverChallenge, that.serverChallenge)) return false;
        return Arrays.equals(serial, that.serial);

    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(clientChallenge);
        result = 31 * result + Arrays.hashCode(serverChallenge);
        result = 31 * result + Arrays.hashCode(serial);
        return result;
    }
}