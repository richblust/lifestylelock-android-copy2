package lock.lifestyle.com.lifestylelock.model;

import android.app.Application;
import android.bluetooth.BluetoothGattService;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;

import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by QSD on 13/Oct/2016.
 * Class is used for
 */

public class Global extends Application {

    public static String TAG = Global.class.getSimpleName();

    // List of currently saved devices
    public static List<DeviceStorage> savedDevices = new ArrayList<>();

    // Currently working device
    public static DeviceStorage currentDevice;


    public static Queue<DeviceStorage> connectionQueue = new LinkedList<DeviceStorage>();


    private static Global instance;


    public static Global get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        instance = this;

        // Stetho library initialization
        Stetho.initializeWithDefaults(this);

    }


}
