package lock.lifestyle.com.lifestylelock.interfaces;

/**
 * Created by QSD on 19/Oct/2016.
 * Class is used for
 */

public interface SwipeHandler {

    void onRename(String deviceAddress);

    void onRemove(String deviceAddress);
}
