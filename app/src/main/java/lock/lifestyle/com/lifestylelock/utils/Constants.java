package lock.lifestyle.com.lifestylelock.utils;

import android.content.IntentFilter;

import java.math.BigInteger;

import lock.lifestyle.com.lifestylelock.BuildConfig;


/**
 * Constant class is used to store global constant values
 */

public class Constants {

    // Flavours
    public final static String craftsmanFlavour = "craftsman";
    public final static String lifestyleFlavour = "lifestyleLock";

    // Scan period in milliseconds
    public static final long SCAN_PERIOD = 5500;

    // Connection states
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;


    // Intents
    public final static String ACTION_GATT_CONNECTED =
            "lock.lifestyle.com.lifestylelock.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "lock.lifestyle.com.lifestylelock.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "lock.lifestyle.com.lifestylelock.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "lock.lifestyle.com.lifestylelock.ACTION_DATA_AVAILABLE";
    public final static String ACTION_LOCK_STATE_QUERY =
            "lock.lifestyle.com.lifestylelock.ACTION_LOCK_STATE_QUERY";
    public final static String ACTION_CLIENT_CHALLENGE_WRITE =
            "lock.lifestyle.com.lifestylelock.ACTION_CLIENT_CHALLENGE_WRITE";
    public final static String ACTION_WRITE_ENCRYPTED_HASH =
            "lock.lifestyle.com.lifestylelock.ACTION_WRITE_ENCRYPTED_HASH";
    public final static String ACTION_AUTHENTIFICATION_CONFIRMED =
            "lock.lifestyle.com.lifestylelock.ACTION_AUTHENTIFICATION_CONFIRMED";
    public final static String ACTION_AUTHENTICATION_MERGE =
            "lock.lifestyle.com.lifestylelock.ACTION_AUTHENTICATION_MERGE";
    public final static String ACTION_LOCK_UNLOCK =
            "lock.lifestyle.com.lifestylelock.ACTION_LOCK_UNLOCK";
    public final static String ACTION_SET_BATTERY_LEVEL =
            "lock.lifestyle.com.lifestylelock.ACTION_SET_BATTERY_LEVEL";
    public final static String ACTION_READ_FIRMWARE =
            "lock.lifestyle.com.lifestylelock.ACTION_READ_FIRMWARE";
    public final static String ACTION_UTC_TIME =
            "lock.lifestyle.com.lifestylelock.ACTION_UTC_TIME";
    public final static String ACTION_UTC_OFFSET =
            "lock.lifestyle.com.lifestylelock.ACTION_UTC_OFFSET";
    public final static String ACTION_LOCK_STATE_CHANGE =
            "lock.lifestyle.com.lifestylelock.ACTION_LOCK_STATE_CHANGE";
    public final static String ACTION_SUBSCRIBE_BATTERY =
            "lock.lifestyle.com.lifestylelock.ACTION_SUBSCRIBE_BATTERY";


    // Lock states
    public static final byte[] LOCK_OR_UNLOCK = BigInteger.valueOf(0x0).toByteArray();
    public static final byte[] LOCK_STATE_LOCKED = BigInteger.valueOf(0x0).toByteArray();
    public static final byte[] LOCK_STATE_TIME_UNLOCK_CLOSED = BigInteger.valueOf(0x1).toByteArray();
    public static final byte[] LOCK_STATE_UNLOCKED_AUTOMATIC_LOCK = BigInteger.valueOf(0x2).toByteArray();
    public static final byte[] LOCK_STATE_UNLOCKED_MANUAL_LOCK = BigInteger.valueOf(0x3).toByteArray();


    // Intent filters
    public static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_GATT_CONNECTED);
        intentFilter.addAction(Constants.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(Constants.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(Constants.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(Constants.ACTION_LOCK_STATE_QUERY);
        intentFilter.addAction(Constants.ACTION_CLIENT_CHALLENGE_WRITE);
        intentFilter.addAction(Constants.ACTION_WRITE_ENCRYPTED_HASH);
        intentFilter.addAction(Constants.ACTION_AUTHENTIFICATION_CONFIRMED);
        intentFilter.addAction(Constants.ACTION_LOCK_UNLOCK);
        intentFilter.addAction(Constants.ACTION_SET_BATTERY_LEVEL);
        intentFilter.addAction(Constants.ACTION_READ_FIRMWARE);
        intentFilter.addAction(Constants.ACTION_UTC_TIME);
        intentFilter.addAction(Constants.ACTION_UTC_OFFSET);
        intentFilter.addAction(Constants.ACTION_LOCK_STATE_CHANGE);
        intentFilter.addAction(Constants.ACTION_AUTHENTICATION_MERGE);
        intentFilter.addAction(Constants.ACTION_SUBSCRIBE_BATTERY);
        return intentFilter;
    }


    // Website url path
    public static String getWebUrl() {
        if (BuildConfig.FLAVOR.equals(Constants.craftsmanFlavour)) {
            return "https://www.craftsman.com/smart-lock";
        } else {
            return "https://lifestylelock.com/";
        }
    }


    // Get default font name
    public static String getFontName() {
        if (BuildConfig.FLAVOR.equals(Constants.craftsmanFlavour)) {
            return "fonts/Teko-Regular.ttf";
        } else {
            return "fonts/Roboto-Regular.ttf";
        }
    }

}
