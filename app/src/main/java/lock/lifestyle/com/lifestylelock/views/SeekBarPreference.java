package lock.lifestyle.com.lifestylelock.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import lock.lifestyle.com.lifestylelock.R;

// http://no-magic.info/development-for-android-os/seekbar-in-preferences.html

public class SeekBarPreference extends Preference implements OnSeekBarChangeListener {

    private final String TAG = getClass().getName();

    private static final int DEFAULT_VALUE = 0;

    private int mMaxValue = 100;
    private int mMinValue = -100;
    private String mLeftText, mRightText;
    private int mInterval = 5;
    public static int mCurrentValue;

    private TextView mLeft, mRight;
    private SeekBar mSeekBar;

    public SeekBarPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPreference(context, attrs);
    }

    /*public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initPreference(context, attrs);
    }*/

    private void initPreference(Context context, AttributeSet attrs) {
        setValuesFromXml(attrs);
        mSeekBar.setMax(mMaxValue - mMinValue);
        mSeekBar.setOnSeekBarChangeListener(this);
    }

    private void setValuesFromXml(AttributeSet attrs) {
        TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.SeekBarPreference, 0, 0);
        mMaxValue = array.getInt(R.styleable.SeekBarPreference_maxValue, 100);
        mMinValue = array.getInt(R.styleable.SeekBarPreference_minValue, -100);
        mLeftText = array.getString(R.styleable.SeekBarPreference_leftText);
        mRightText = array.getString(R.styleable.SeekBarPreference_rightText);
        array.recycle();
        init();
    }

    private void init() {
        LayoutInflater mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout layout = (RelativeLayout) mInflater.inflate(R.layout.preference_seekbar, null, false);

        mLeft = (TextView) layout.findViewById(R.id.seekBarPrefValueLeft);
        mRight = (TextView) layout.findViewById(R.id.seekBarPrefValueRight);
        mSeekBar = (SeekBar) layout.findViewById(R.id.seekBarPrefBar);
        mSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        RelativeLayout layout = null;

        LayoutInflater mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (RelativeLayout) mInflater.inflate(R.layout.preference_seekbar, parent, false);

        mLeft = (TextView) layout.findViewById(R.id.seekBarPrefValueLeft);
        mRight = (TextView) layout.findViewById(R.id.seekBarPrefValueRight);
        mSeekBar = (SeekBar) layout.findViewById(R.id.seekBarPrefBar);
        mSeekBar.setOnSeekBarChangeListener(this);

        return layout;
    }

    @Override
    public void onBindView(View view) {
        super.onBindView(view);
        try {
            ViewParent oldContainer = mSeekBar.getParent();
            ViewGroup newContainer = (ViewGroup) view.findViewById(R.id.seekBarPrefBarContainer);
            if (oldContainer != newContainer) {
                if (oldContainer != null) {
                    ((ViewGroup) oldContainer).removeView(mSeekBar);
                }
                newContainer.removeAllViews();
                newContainer.addView(mSeekBar, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Error binding view: " + ex.toString());
        }
        updateView(view);
    }

    protected void updateView(View view) {
        try {
            RelativeLayout layout = (RelativeLayout) view;
            /*mStatusText = (TextView) layout.findViewById(R.id.seekBarPrefValue);
            mStatusText.setText(String.valueOf(mCurrentValue));
            mStatusText.setMinimumWidth(30);*/
            mSeekBar.setProgress(mCurrentValue - mMinValue);
            /*TextView unitsRight = (TextView) layout.findViewById(R.id.seekBarPrefUnitsRight);
            TextView unitsLeft = (TextView) layout.findViewById(R.id.seekBarPrefUnitsLeft);*/
        } catch (Exception e) {
            Log.e(TAG, "Error updating seek bar preference", e);
        }

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int newValue = progress + mMinValue;
        if (newValue > mMaxValue)
            newValue = mMaxValue;
        else if (newValue < mMinValue)
            newValue = mMinValue;
        else if (mInterval != 1 && newValue % mInterval != 0)
            newValue = Math.round(((float) newValue) / mInterval) * mInterval;
        if (!callChangeListener(newValue)) {
            seekBar.setProgress(mCurrentValue - mMinValue);
            return;
        }
        mCurrentValue = newValue;
        // mStatusText.setText(String.valueOf(newValue));
        persistInt(progress);
        int TakeValue = mCurrentValue;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        notifyChanged();
    }

    @Override
    protected Object onGetDefaultValue(TypedArray ta, int index) {
        int defaultValue = ta.getInt(index, DEFAULT_VALUE);
        return defaultValue;
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {

        if (restoreValue) {
            mCurrentValue = getPersistedInt(mCurrentValue);
        } else {
            int temp = 0;
            try {
                temp = (Integer) defaultValue;
            } catch (Exception ex) {
                Log.e(TAG, "Invalid default value: " + defaultValue.toString());
            }

            persistInt(temp);
            mCurrentValue = temp;
        }
    }

}
