package lock.lifestyle.com.lifestylelock.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import lock.lifestyle.com.lifestylelock.R;

public class CustomTextPreference extends Preference {

    private boolean mAccentColored, mSmallText;
    private String preferenceTitle;

    public CustomTextPreference(Context context) {
        super(context);
    }

    public CustomTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomTextPreference, 0, 0);
        mAccentColored = array.getBoolean(R.styleable.CustomTextPreference_accentColor, false);
        mSmallText = array.getBoolean(R.styleable.CustomTextPreference_smallText, false);
        preferenceTitle = array.getString(R.styleable.CustomTextPreference_android_title);
        array.recycle();
    }

    @Override
    protected void onBindView(View parent) {

        super.onBindView(parent);
        TextView preferenceTextView = (TextView) parent.findViewById(android.R.id.title);
        if (mAccentColored) {
            preferenceTextView.setTextColor(getContext().getColor(R.color.orange));
        }
        if (mSmallText) {
            preferenceTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            preferenceTextView.setGravity(Gravity.TOP);
        }
        preferenceTextView.setText(preferenceTitle);

    }

}
