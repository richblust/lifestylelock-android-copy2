package lock.lifestyle.com.lifestylelock.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;

import lock.lifestyle.com.lifestylelock.model.Global;
import lock.lifestyle.com.lifestylelock.utils.CharacteristicHelper;
import lock.lifestyle.com.lifestylelock.utils.Constants;
import lock.lifestyle.com.lifestylelock.utils.Converter;
import lock.lifestyle.com.lifestylelock.utils.HashComponents;
import lock.lifestyle.com.lifestylelock.utils.Validators;

import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_AUTHENTICATION_CHALLENGE;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_AUTHENTICATION_HASH;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_BATTERY;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_FIRMWARE;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_HARDWARE_VERSION;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_LOCK_QUERY;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_TIME_SYNC;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_TIME_ZONE_OFFSET;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LC_UNLOCK;
import static lock.lifestyle.com.lifestylelock.model.GattUUID.LD_DESCRIPTOR;
import static lock.lifestyle.com.lifestylelock.model.Global.connectionQueue;
import static lock.lifestyle.com.lifestylelock.model.Global.currentDevice;

/**
 * Created by QSD on 9/26/2016.
 */

public class BluetoothLeService extends Service {
    private final static String TAG = BluetoothLeService.class.getSimpleName();
    private final IBinder mBinder = new LocalBinder();
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = Constants.STATE_DISCONNECTED;
    private HashComponents components;
    List<BluetoothGattService> services;

    private static BluetoothLeService instance = null;

    public static boolean isInstanceCreated() {
        return instance != null;
    }

    @Override
    public void onCreate() {
        instance = this;
    }

    @Override
    public void onDestroy() {
        instance = null;
    }


    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = Constants.ACTION_GATT_CONNECTED;

                mConnectionState = Constants.STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());


            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = Constants.ACTION_GATT_DISCONNECTED;
                mConnectionState = Constants.STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                if(gatt.getDevice()!=null) {
                    broadcastUpdate(intentAction, gatt.getDevice().getAddress());
                }else{
                    broadcastUpdate(intentAction,currentDevice.getMacAddress());
                }
            }
        }


        // this will get called after the client initiates a BluetoothGatt.discoverServices() call
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(Constants.ACTION_GATT_SERVICES_DISCOVERED);

                services = gatt.getServices();
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }


        //Callback reporting the result of a characteristic read operation.
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int
                status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.i(TAG, "Characteristic is read successfully");

                switch (characteristic.getUuid().toString()) {

                    // Reading client challenge, generate hash and continue to write random challenge to device
                    case LC_AUTHENTICATION_CHALLENGE:
                        if (!currentDevice.isAuthenticated()) {
                            generateHash(characteristic);
                            broadcastUpdate(Constants.ACTION_CLIENT_CHALLENGE_WRITE);
                        }
                        break;

                    case LC_AUTHENTICATION_HASH:

                        if (!currentDevice.isAuthenticated()) {
                            HashComponents decrypted = HashComponents.decrypt(characteristic.getValue(), false);
                            if (decrypted != null && decrypted.equals(components)) {
                                Log.i(TAG, "Authentication is successful");
                                currentDevice.setAuthenticated(true);
                            }

                            // Confirm validation
                            Log.i(TAG, "Client hash is written to lock");
                            broadcastUpdate(Constants.ACTION_AUTHENTIFICATION_CONFIRMED, currentDevice.getMacAddress());

                            break;
                        }


                        // Battery level is read, now set current state and read firmware
                    case LC_BATTERY:
                        currentDevice.setBatteryLevel(characteristic.getValue());
                        broadcastUpdate(Constants.ACTION_SET_BATTERY_LEVEL);
                        break;


                    // Firmware is read now set firmware and continue to read hardware version
                    case LC_FIRMWARE:
                        try {
                            currentDevice.setFirmware(new String(characteristic.getValue(), "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Log.e(TAG, "Problem to convert device firmware");
                        }
                        broadcastUpdate(Constants.ACTION_READ_FIRMWARE);
                        break;


                    // Set hardware version and continue to write UTC time to device
                    case LC_HARDWARE_VERSION:
                        try {
                            currentDevice.setHardware(new String(characteristic.getValue(), "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Log.e(TAG, "Problem to convert device hardware version");
                        }
                        broadcastUpdate(Constants.ACTION_UTC_TIME);
                        break;


                    // Read lock state and update status of global device
                    case LC_LOCK_QUERY:
                        Log.i(TAG, "Lock state is read. Lock state is " + Converter.byteArrayToHexString
                                (characteristic.getValue()));

                        currentDevice.setDeviceLockState(characteristic.getValue());
                        Validators.updateDeviceStatus(currentDevice);
                        broadcastUpdate(Constants.ACTION_LOCK_STATE_CHANGE);
                        break;

                }

            }

        }


        //Callback indicating the result of a characteristic write operation.
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int
                status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.i(TAG, "Writing characteristic is successful");


                switch (characteristic.getUuid().toString()) {

                    // Write client hash to lock in case when challenge is obtained, now lets write encrypted hash
                    case LC_AUTHENTICATION_CHALLENGE:
                        Log.i(TAG, "Client hash is obtained");
                        broadcastUpdate(Constants.ACTION_WRITE_ENCRYPTED_HASH, currentDevice.getMacAddress());
                        break;


                    // Hash is written now lets decrypt and compare hashes
                    case LC_AUTHENTICATION_HASH:
                        broadcastUpdate(Constants.ACTION_AUTHENTICATION_MERGE, currentDevice.getMacAddress());
                        break;


                    // UTC time is written now lets write UTC OFFSET
                    case LC_TIME_SYNC:
                        Log.i(TAG, "UTC Time written!");
                        broadcastUpdate(Constants.ACTION_UTC_OFFSET);
                        break;

                    // UTC offset written now lock/unlock device action
                    case LC_TIME_ZONE_OFFSET:
                        Log.i(TAG, "UTC Time offset written!");
                        broadcastUpdate(Constants.ACTION_LOCK_UNLOCK, currentDevice.getMacAddress());
                        break;

                    // Device is locked or unlocked now lets change lock state on image
                    case LC_UNLOCK:
                        broadcastUpdate(Constants.ACTION_LOCK_STATE_QUERY, gatt.getDevice().getAddress());
                        break;
                }

            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.i(TAG, "Writing descriptor is successful");

                switch (descriptor.getCharacteristic().getUuid().toString()) {
                    case LC_LOCK_QUERY:
                        if (LD_DESCRIPTOR.equals(descriptor.getUuid().toString())) {
                            Log.i("Notification", "LC_LOCK_QUERY descriptor is written successfully");
                            broadcastUpdate(Constants.ACTION_SUBSCRIBE_BATTERY);
                        }
                        break;

                    case LC_BATTERY:
                        if (LD_DESCRIPTOR.equals(descriptor.getUuid().toString())) {
                            Log.i("Notification", "LC_BATTERY descriptor is written successfully");
                            //broadcastUpdate(Constants.ACTION_SUBSCRIBE);
                        }
                        break;


                }
            }

        }

        //Callback triggered as a result of a remote characteristic notification.
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            byte[] data = characteristic.getValue();

            switch (characteristic.getUuid().toString()) {
                case LC_LOCK_QUERY:
                    broadcastUpdate(Constants.ACTION_DATA_AVAILABLE, data,LC_LOCK_QUERY,currentDevice.getMacAddress());
                    Log.e("Notification", "Lock query");
                    break;

//                case LC_BATTERY:
//
//                    broadcastUpdate(Constants.ACTION_DATA_AVAILABLE, data,LC_BATTERY,currentDevice.getMacAddress());
//                    Log.e("Notification", "Battery log");
//                    break;

            }



        }


        //Callback invoked when a reliable write transaction has been completed.
        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
        //return super.onStartCommand(intent, flags, startId);
    }

    public void writeCharacteristic(byte[] value, String serviceUUID, String characteristicUUID) {
        CharacteristicHelper.writeCustomCharacteristic(value, mBluetoothAdapter, mBluetoothGatt, serviceUUID,
                characteristicUUID);
    }

    public void readCharacteristic(String serviceUUID, String characteristicUUID) {
        CharacteristicHelper.readCustomCharacteristic(mBluetoothAdapter, mBluetoothGatt, serviceUUID,
                characteristicUUID);
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, String deviceAddress) {
        final Intent intent = new Intent(action);
        intent.putExtra("device_address", deviceAddress);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, byte[] data,String actionName,String address) {
        final Intent intent = new Intent(action);
        intent.putExtra("byteData", data);
        intent.putExtra("action_name", actionName);
        intent.putExtra("device_address", address);
        sendBroadcast(intent);
    }


    private void generateHash(BluetoothGattCharacteristic characteristic) {
        currentDevice.setServerChallenge(characteristic.getValue());

        components = new HashComponents(currentDevice.getClientChallenge(), currentDevice.getServerChallenge(),
                currentDevice.getSerialByte());

        currentDevice.setEncryptedHash(components.encrypt(true));

    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;

    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }


        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = Constants.STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        //if(mBluetoothGatt != null) mBluetoothGatt.close();

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        //refreshDeviceCache(mBluetoothGatt);

        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }


        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Log.e(TAG,"Connection thread sleep error");

        }
        // this will get called when a device connects or disconnects
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = Constants.STATE_CONNECTING;
        return true;
    }





    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt,
     * android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }


        boolean isSetNotificationSuccess = mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
        if (isSetNotificationSuccess) {
            Log.e("Notification", "Set notification success!");
        } else {
            Log.e("Notification failed", "Set notification failed!");
        }

        if (UUID.fromString(LC_LOCK_QUERY).equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = getDescriptor(characteristic, LD_DESCRIPTOR);

//            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
            if(descriptor!=null) {
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

                boolean isWriteDescriptorSuccess = mBluetoothGatt.writeDescriptor(descriptor);


                if (isWriteDescriptorSuccess) {
                    Log.e("Notification", "Write descriptor success! (Lock)");
                } else {
                    Log.e("Notification failed", "Write descriptor failed!(Lock)");
                }
            }else{Log.e("Notification","Descriptor is null (Battery)");}
        }


//        if (UUID.fromString(LC_BATTERY).equals(characteristic.getUuid())) {
//            BluetoothGattDescriptor descriptor = getDescriptor(characteristic, LD_DESCRIPTOR);
//            if(descriptor!=null) {
//                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//
//                boolean isWriteDescriptorSuccess = mBluetoothGatt.writeDescriptor(descriptor);
//
//                if (isWriteDescriptorSuccess) {
//                    Log.e("Notification", "Write descriptor success!(Battery)");
//                } else {
//                    Log.e("Notification failed", "Write descriptor failed!(Battery)");
//                }
//            }else{Log.e("Notification","Descriptor is null (Battery)");}
//        }
    }

    public BluetoothGattDescriptor getDescriptor(BluetoothGattCharacteristic characteristic, String descriptorUUID) {
        BluetoothGattDescriptor descriptor = null;
        descriptor = characteristic.getDescriptor(UUID.fromString(descriptorUUID));

        if (descriptor == null) {
            Log.e("Notification", "getDescriptor is NULL");
        } else {
            Log.e("Notification", "getDescriptor is success");
        }

        return descriptor;
    }


    public BluetoothGattCharacteristic getCharacteristicFromServices(String serviceUUID, String characteristicUUID) {

        BluetoothGattService _service = null;
        BluetoothGattCharacteristic _characteristic = null;

        for (int i = 0; i < services.size(); i++) {
            if (services.get(i).getUuid().equals(UUID.fromString(serviceUUID))) {
                _service = services.get(i);
                for (int j = 0; j < services.get(i).getCharacteristics().size(); j++) {
                    if (services.get(i).getCharacteristics().get(j).getUuid().equals(UUID.fromString
                            (characteristicUUID))) {
                        _characteristic = services.get(i).getCharacteristics().get(j);
                    }
                }
            }
        }

        if (_characteristic == null) {
            Log.e("Notification failed", "Characteristic DO NOT exist");
        } else {
            Log.e("Notification", "Success characteristic exist");
        }

        if (_service == null) {
            Log.e("Notification failed", "Service DO NOT exist");
        } else {
            Log.e("Notification", "Success service exist");
        }


        return _characteristic;

    }


    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    private boolean refreshDeviceCache(BluetoothGatt gatt) {
        try {
            BluetoothGatt localBluetoothGatt = gatt;
            Method localMethod = localBluetoothGatt.getClass().getMethod("refresh", new Class[0]);
            if (localMethod != null) {
                boolean bool = ((Boolean) localMethod.invoke(localBluetoothGatt, new Object[0])).booleanValue();
                return bool;
            }
        } catch (Exception localException) {
            Log.e(TAG, "An exception occured while refreshing device");
        }
        return false;
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    public static boolean waitIdle() {
        int i = 300;
        i /= 10;
        while (--i > 0) {
            if (true)
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }

        return i > 0;
    }

}
