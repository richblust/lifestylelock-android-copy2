package lock.lifestyle.com.lifestylelock;

import org.junit.Test;

import java.util.Arrays;

import lock.lifestyle.com.lifestylelock.utils.HashComponents;

import static org.junit.Assert.*;

/**
 * Created by nick on 16/10/2016.
 */

public class HashComponentsTest {
    @Test
    public void testHexToData() throws Exception {
        assertArrayEquals(HashComponents.hexStringToByteArray("abcdef1234567890"), HashComponents.toBytes(0xab,0xcd,0xef,0x12,0x34,0x56,0x78,0x90));
    }

    @Test
    public void init_components() throws Exception {
        byte[] clientChallenge = HashComponents.generateClientChallenge();
        assertEquals(clientChallenge.length, 2);
        byte[] serverChallenge = HashComponents.generateClientChallenge();
        byte[] serial = HashComponents.toBytes(0xAB, 0xCD, 0xAB, 0xCD, 0xAB, 0xCD);

        HashComponents c = new HashComponents(clientChallenge, serverChallenge, serial);
        assertArrayEquals(clientChallenge, c.clientChallenge);
        assertArrayEquals(serverChallenge, c.serverChallenge);
        assertArrayEquals(serial, c.serial);
    }

    @Test
    public void testEquals() throws Exception {
        byte[] clientChallenge = HashComponents.generateClientChallenge();
        assertEquals(clientChallenge.length, 2);
        byte[] serverChallenge = HashComponents.generateClientChallenge();
        byte[] serial = HashComponents.toBytes(0xAB,0xCD,0xAB,0xCD,0xAB,0xCD);

        HashComponents c = new HashComponents(clientChallenge, serverChallenge, serial);
        HashComponents c2 = new HashComponents(clientChallenge, serverChallenge, serial);
        assertArrayEquals(clientChallenge, c.clientChallenge);
        assertArrayEquals(serverChallenge, c.serverChallenge);
        assertArrayEquals(serial, c.serial);
        assertArrayEquals(c.clientChallenge, c2.clientChallenge);
        assertArrayEquals(c.serverChallenge, c2.serverChallenge);
        assertArrayEquals(c.serial, c2.serial);
        assertEquals(c, c2);
        assertEquals(c.hashCode(), c2.hashCode());
    }

    @Test
    public void testHash() throws Exception {
        byte[] clientChallenge = HashComponents.generateClientChallenge();
        assertEquals(clientChallenge.length, 2);
        byte[] serverChallenge = HashComponents.generateClientChallenge();
        byte[] serial = HashComponents.toBytes(0xAB,0xCD,0xAB,0xCD,0xAB,0xCD);

        HashComponents c = new HashComponents(clientChallenge, serverChallenge, serial);

        byte[] hash = c.toHashData(true);
        assertEquals(hash.length, 16);
        assertArrayEquals(clientChallenge, Arrays.copyOfRange(hash,0,2));
        assertArrayEquals(serverChallenge, Arrays.copyOfRange(hash,2,4));
        assertArrayEquals(serial, Arrays.copyOfRange(hash,4,10));
        byte[] empty = {0,0,0,0,0,0};
        assertArrayEquals(empty, Arrays.copyOfRange(hash, 10, 16));

        hash = c.toHashData(false); // server hash arrangement
        assertEquals(hash.length, 16);
        assertArrayEquals(serverChallenge, Arrays.copyOfRange(hash,0,2));
        assertArrayEquals(clientChallenge, Arrays.copyOfRange(hash,2,4));
        assertArrayEquals(serial, Arrays.copyOfRange(hash,4,10));
        assertArrayEquals(empty, Arrays.copyOfRange(hash, 10, 16));
    }

    @Test
    public void testFromHash() throws Exception {
        byte[] clientChallenge = HashComponents.generateClientChallenge();
        assertEquals(clientChallenge.length, 2);
        byte[] serverChallenge = HashComponents.generateClientChallenge();
        byte[] serial = HashComponents.toBytes(0xAB,0xCD,0xAB,0xCD,0xAB,0xCD);

        HashComponents c = new HashComponents(clientChallenge, serverChallenge, serial);

        byte[] hash = c.toHashData(true);

        HashComponents fromClientHash = new HashComponents(hash, true);
        assertEquals(c, fromClientHash);
    }

    @Test
    public void testEncryptDecrypt() throws Exception {
        byte[] clientChallenge = HashComponents.generateClientChallenge();
        assertEquals(clientChallenge.length, 2);
        byte[] serverChallenge = HashComponents.generateClientChallenge();
        byte[] serial = HashComponents.toBytes(0xAB,0xCD,0xAB,0xCD,0xAB,0xCD);

        HashComponents c = new HashComponents(clientChallenge, serverChallenge, serial);

        byte[] encrypted = c.encrypt(true);
        assertEquals(encrypted.length, 16);

        HashComponents decrypted = HashComponents.decrypt(encrypted, true);
        assertNotNull(decrypted);
        assertEquals(c, decrypted);
        assertEquals(c.hashCode(), decrypted.hashCode());
    }
}